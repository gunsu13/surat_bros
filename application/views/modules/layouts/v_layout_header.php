<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?=$page_title?> | Sistem Informasi Management Surat Menyurat</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/adminlte.min.css">
    <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">  
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/daterangepicker/daterangepicker.css">
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
</head>
<body class="hold-transition sidebar-collapse layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="<?=base_url()?>dashboard" class="navbar-brand">
        <img src="<?=base_url()?>assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">SIMSURAT</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Menu</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2): ?>
                <li><a href="<?=base_url()?>surat/suratMasuk" class="dropdown-item">Surat Masuk </a></li>
              <?php endif ?>
              <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2): ?>
                <li><a href="<?=base_url()?>surat/suratKeluar" class="dropdown-item">Surat Keluar</a></li>
              <?php endif ?>
              <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2): ?>
                <li><a href="<?=base_url()?>surat/arsipSuratMasuk" class="dropdown-item">Arsip Surat</a></li>
              <?php endif ?>


              <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2 or $this->session->userdata('level') == 3 or $this->session->userdata('level') == 4): ?>
                 <!-- Level two dropdown-->
                <li class="dropdown-submenu dropdown-hover">
                  <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Disposisi</a>
                  <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                  <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2 or $this->session->userdata('level') == 3): ?>
                    <li><a href="<?=base_url()?>surat/disposisiDirektur" class="dropdown-item">Disposisi Direktur</a></li>
                  <?php endif ?>
                  <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2): ?>
                    <li><a href="<?=base_url()?>surat/disposisi" class="dropdown-item">Disposisi ( Distribusi )</a></li>
                  <?php endif ?>
                  <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2 or $this->session->userdata('level') == 4): ?>
                    <li><a href="<?=base_url()?>surat/disposisiKonfirmasi" class="dropdown-item">Disposisi Unit</a></li>
                  <?php endif ?>
                  </ul>
                </li>
              <?php endif ?>


              <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2): ?>
              
                <li class="dropdown-divider"></li>
                <!-- Level two dropdown-->
                <li class="dropdown-submenu dropdown-hover">
                  <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Master Data</a>
                  <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li><a href="<?=base_url()?>master/divisi" class="dropdown-item">Divisi</a></li>
                    <li><a href="<?=base_url()?>master/unit" class="dropdown-item">Unit</a></li>
                    <li><a href="<?=base_url()?>master/instansi" class="dropdown-item">Instansi</a></li>
                    <li><a href="<?=base_url()?>master/keteranganDisposisi" class="dropdown-item">Keterangan Disposisi</a></li>
                    <li><a href="<?=base_url()?>master/user" class="dropdown-item">User</a></li>
                  </ul>
                </li>
                <!-- End Level two -->
              <?php endif ?>

              <?php if ($this->session->userdata('level') == 1 or $this->session->userdata('level') == 2): ?>
                <!-- Level two dropdown-->
                <li class="dropdown-submenu dropdown-hover">
                  <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Laporan</a>
                  <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                    <li><a href="<?=base_url()?>report/surat_masuk" class="dropdown-item">Laporan Surat Masuk</a></li>
                    <li><a href="<?=base_url()?>report/surat_keluar" class="dropdown-item">Laporan Surat Keluar</a></li>
                    <li><a href="<?=base_url()?>report/disposisi" class="dropdown-item">Laporan Disposisi</a></li>
                    <li><a href="<?=base_url()?>report/disposisi_konfirmasi" class="dropdown-item">Laporan Konfirmasi Disposisi</a></li>
                    <li><a href="<?=base_url()?>report/arsip_surat_masuk" class="dropdown-item">Laporan Arsip Surat</a></li>
                  </ul>
                </li>
                <!-- End Level two -->
              <?php endif ?>
            </ul>
          </li>
          <?php if ($this->session->userdata('level') == 1): ?>
            <li class="nav-item">
              <a href="<?=base_url()?>sistem/setting" class="nav-link">System Setting</a>
            </li>
          <?php endif ?>
        </ul>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <li class="nav-item">
          <a href="<?=base_url()?>logout" class="nav-link">Logout</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
              class="fas fa-th-large"></i></a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?=base_url()?>assets/dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SIMSURAT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?=base_url()?>assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Divisi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Instansi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Unit</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Surat
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Keluar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Masuk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Disposisi</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="../widgets.html" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Widgets
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
