<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Surat</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-12">
    				<div class="card">
    					<div class="card-body">
    						<div class="row">
    							<div class="col-md-12">
    								<form role="form" id="formInput">
    									<div class="row">
    										<div class="col-md-6">
    											<div class="form-group">
    												<label>Sebelumnya</label>
    												<input type="text" class="form-control" name="skNomorSuratPrev" id="skNomorSuratPrev" readonly="">
    											</div>
    										</div>
    										<div class="col-md-6">
    											<div class="form-group">
    												<label for="skNomorSurat" id="lbNoSuratKeluar">No Surat Keluar</label>
    												<input type="hidden" name="skKode" id="skKode">
    												<input type="text" class="form-control" name="skNomorSurat" id="skNomorSurat" autocomplete="off">
    											</div>
    										</div>
    										<div class="col-md-6">
    											<div class="form-group">
    												<label>Jenis Surat</label>
    												<select class="form-control" name="skJenis" id="skJenis">
    													<option value="">- Pilih Jenis -</option>
    													<?php foreach ($jenis_surats as $jenis_surat): ?>
    														<option value="<?=$jenis_surat->skjID?>"><?=$jenis_surat->skjNama?></option>
    													<?php endforeach ?>
    												</select>
    											</div>
    										</div>
    										<div class="col-md-6">
    											<div class="form-group">
    												<label>Tujuan Surat/ Instansi</label>
    												<select class="form-control select2" name="instansiKode" id="instansiKode">
    													<option value="">- Pilih Instansi -</option>
    													<?php foreach ($instansis as $inst): ?>
    														<option value="<?=$inst->instansiKode?>"><?=$inst->instansiNama?></option>
    													<?php endforeach ?>
    												</select>
    											</div>
    										</div>
    										<div class="col-md-6">
    											<div class="form-group">
    												<label for="skTanggalKeluar">Tanggal Surat</label>
    												<input type="text" class="form-control" name="skTanggalKeluar" id="skTanggalKeluar" autocomplete="off">
    											</div>
    										</div>
    										<div class="col-md-6">

    											<div class="form-group">
    												<label for="skPerihal">Perihal</label>
    												<input type="text" class="form-control" name="skPerihal" id="skPerihal">
    											</div>
    										</div>
    										<div class="col-md-12">

    											<div class="form-group">
    												<label for="skTujuan">Tujuan</label>
    												<input type="text" class="form-control" name="skTujuan" id="skTujuan">
    											</div>
    										</div>
    										<div class="col-md-12">

    											<div class="form-group">
    												<label for="skKeterangan">Notes</label>
    												<textarea class="form-control" rows="3" name="skKeterangan" id="skKeterangan"></textarea>
    											</div>
    										</div>
    									</div>
    								</form>
    							</div>
    						</div>
    						<hr>
    						<button type="button" class="btn btn-default" id="btnMdDivisi">
    							List Surat Keluar
    						</button>
    						<div class="btn btn-group float-right">

    							<button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
    							<button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
    						</div>
    					</div>
    				</div>
    			</div>
    			<!-- /.col-md-6 -->
    		</div>
    		<!-- /.row -->
    	</div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal -->
    <div class="modal fade" id="mdInstansi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Surat Keluar</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
			  <div class="row">
									<div class="col-md-12">
										Filter
									</div>
								</div>
		  <div class="row">
			  
            	<div class="col-md-4">
					<div class="form-group">
						<label>Jenis Surat</label>
						<select class="form-control" name="skjID" id="skjID">
							<option value="-">- Pilih Jenis -</option>
							<?php foreach ($jenis_surats as $jenis_surat): ?>
								<option value="<?=$jenis_surat->skjID?>"><?=$jenis_surat->skjNama?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<label>Tahun Surat</label>
					<select class="form-control" name="tahun_surat" id="tahun_surat">
						<option value="<?php echo date('Y'); ?>">- Pilih Tahun -</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
						<option value="2023">2023</option>
					</select>
				</div>
            </div>
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

    <!-- modal -->
    <div class="modal fade" id="mdFile">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">File Surat</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view2">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">

	var edit = false;
	var jenisSurat = '-';
	var tahun_surat = $('#tahun_surat').val();

	$('#skTanggalKeluar').datepicker({
      autoclose: true
    })

	$('#skjID').change(function(){
		jenisSurat = $(this).val();
		getDataTabel(jenisSurat, tahun_surat);
	})

	$('#tahun_surat').change(function(){
		tahun_surat = $(this).val();
		getDataTabel(jenisSurat, tahun_surat);
	})

    $('#skJenis').change(function(){
    	var skjID = $(this).val();
    	getDataSuratByJenisDetil(skjID);
    })

	$('body').on('click', '.btnFileView', function(){
		var reffKode = $(this).attr('id');
		getDataFile(reffKode);

		$('#mdFile').modal('show');
	})

	$('.btnInputFile').click(function(){
		var kodeSurat = $(this).attr('id');
		window.location.href = '<?=base_url()?>surat/suratMasukFile/'+kodeSurat;
	})

	$('#btnHapus').click(function(){
		var suratKode = $('#skKode').val();

		if (suratKode == '') 
		{
			$("#formInput")[0].reset();
		}else{
			Swal.fire({
			  title: 'Apakah anda yakin?',
			  text: "Data yang dihapus akan hilang dari list",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Hapus !'
			}).then((result) => {
			  if (result.value) {

			  	$.ajax({
			        url: '<?=base_url()?>surat/suratKeluarDelete/'+suratKode,
			        type: 'GET',
			        dataType: 'html',
	        		async: false
			    }) 
			    .done(function(data) {
			    	console.log(data);
			     	var obj = JSON.parse(data);

			     	if (obj.status == 'true') 
			     	{
			     		edit = false;
			     		$("#formInput")[0].reset();

			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}else{
			     		Swal.fire(
						  'Info',
						  obj.message,
						  'warning'
						);
			     	}
			    })
			  	.fail(function (jqXHR, textStatus, error) {
			      	console.log("Post error: " + error);
			  	});			  
			  }
			})
		}
	})

	$('body').on('click', '.btnSelectData', function(){
		var suratKode = $(this).attr('id');
		var data = getDataSinggle(suratKode);
		var obj = JSON.parse(data);

		$('#skKode').val(obj.skKode);
		$('#skNomorSurat').val(obj.skNomorSurat);
		$('#skJenis').val(obj.skjID);
		$('#skPerihal').val(obj.skPerihal);
		$('#skTanggalKeluar').val(obj.dateConverted_tanggalSurat);
		$('#skTujuan').val(obj.skTujuan);
		$('#instansiKode').val(obj.kodePenerima);
		$('#skKeterangan').val(obj.skKeterangan);

		edit = true;

		$('#mdInstansi').modal('hide');
	})

	$('#btnMdDivisi').click(function(){
		getDataTabel(jenisSurat, tahun_surat);
		$('#mdInstansi').modal('show');
	})

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInput').serialize();

		if (edit == false) 
		{
			$.ajax({
		        url: '<?=base_url()?>surat/suratKeluarCreate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}else{
			$.ajax({
		        url: '<?=base_url()?>surat/suratKeluarUpdate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
					
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}
	})

	function getDataTabel(jenisSurat, tahun_surat)
	{
		$.get('<?=base_url()?>surat/suratKeluarGetDataTabel/'+jenisSurat+'/'+tahun_surat, function(data){
			$('#data_view').html(data);
		})
	}

	function getDataSinggle(suratKode)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>surat/suratKeluarGetDataSinggle/'+suratKode,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}

	function getDataFile(reffKode)
	{
		$.get('<?=base_url()?>surat/fileUploaderGetDataTb/'+reffKode, function(data){
			$('#data_view2').html(data);
		})
	}

	function getDataSuratByJenisDetil(skjID)
	{
		$.get('<?=base_url()?>surat/suratKeluarGetDataDetilByJenis/'+skjID, function(data){
			var obj = JSON.parse(data);
			console.log(obj);
			if (obj.status == 'false') 
			{
				$('#skNomorSuratPrev').val('');
				getDataSuratKeluarAutoFormatingNomorSurat('', skjID);
			}else{

				$('#skNomorSuratPrev').val(obj[0].skNomorSurat);
				getDataSuratKeluarAutoFormatingNomorSurat(obj[0].skNomorSurat, obj[0].skjID);
			}
			
		})
	}

	function getDataSuratKeluarAutoFormatingNomorSurat(skNomorSuratPrev, skjID)
	{
		$.ajax({
			url: '<?=base_url()?>surat/suratKeluarGetDataAutoFormatingNomorSurat',
			type: 'POST',
			dataType: 'text',
			data: 'skNomorSuratPrev='+skNomorSuratPrev+'&skjID='+skjID
		}) 
		.done(function(data) {
			var obj = JSON.parse(data);
			console.log(obj);

			if (obj.status == 'true') 
			{
				$('#skNomorSurat').val(obj.next);
				$('#lbNoSuratKeluar').html('No Surat Keluar ( *Disarankan )');
			}

		})
		.fail(function (jqXHR, textStatus, error) {
			console.log("Post error: " + error);
		});
	}
</script>
