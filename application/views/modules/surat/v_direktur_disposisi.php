<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Surat</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                  	<div id="data_view">
                  		
                  	</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal -->
    <div class="modal fade" id="mdInstansi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Disposisi Dirut</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<form id="formKonfirmasiDisposisi">
                  <div class="row">
                    <div class="col-md-12">
                      <table class="table" style="font-size: 13px;">
                        <tr>
                          <td width="15%">Nomor Surat</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smNomorSurat"></td>
                          <td width="15%">Tanggal Terima</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTanggalTerima"></td>
                        </tr>
                         <tr>
                          <td width="15%">Tanggal Surat</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTanggalSurat"></td>
                          <td width="15%">Keterangan</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smKeterangan"></td>
                        </tr>
                         <tr>
                          <td width="15%">Perihal</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smPerihal"></td>
                          <td width="15%">Instansi Pengirim</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="instansiNama"></td>
                        </tr>
                        <tr>
                          <td width="15%">Tujuan</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTujuan"></td>
                          <td width="15%">File</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="file"></td>
                        </tr>
                       
                      </table>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Catatan / Keterangan Disposisi</label>
                      <input type="hidden" name="smKode" id="smKode">
                      <textarea name="keterangan" class="form-control" id="keterangan"></textarea>
                    </div>
                  </div>  
                </form>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button type="button" class="btn btn-primary" id="btnKonfirmasiDisposisiFinal">Simpan</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

	<!-- modal -->
    <div class="modal fade" id="mdFile">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">File Surat</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view2">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

<?php $this->load->view('modules/layouts/v_layout_footer'); ?>

<script type="text/javascript">

	getDataSuratMasuk();
  
  $('#btnKonfirmasiDisposisiFinal').click(function(){
    var dataSerialize = $('#formKonfirmasiDisposisi').serialize();

    $.ajax({
        url: '<?=base_url()?>surat/disposisiDirekturUpdate',
        type: 'POST',
        dataType: 'text',
        data: dataSerialize
    }) 
    .done(function(data) {
      console.log(data);
      var obj = JSON.parse(data);

      if (obj.status == 'true') 
      {
        edit = false;
        $("#formKonfirmasiDisposisi")[0].reset();
        $('#mdInstansi').modal('hide');
        getDataSuratMasuk();
        Swal.fire(
          'Info',
          obj.message,
          'success'
        );
      }else{
        Swal.fire(
          'Info',
          obj.message,
          'warning'
        );
      }
    })
    .fail(function (jqXHR, textStatus, error) {
      console.log("Post error: " + error);
    });
  })

  $("#unitID").change(function(){
    unitID = $(this).val();
    getDataKonfirmasi(unitID);
  });
	
  $('body').on('click', '.btnMdDisposisi', function(){
    var smKode = $(this).attr('id');
    getDataSuratMasukDetil(smKode);
    $('#mdInstansi').modal('show');
  })

  $('body').on('click', '.btnFileView', function(){
		var reffKode = $(this).attr('id');
		getDataFile(reffKode);

		$('#mdFile').modal('show');
	})

	function getDataSuratMasuk()
	{
		$.get('<?=base_url()?>surat/disposisiDirekturGetDataTabel/', function(data){
			$('#data_view').html(data);
		})
	}

  function getDataSuratMasukDetil(smKode)
  {
    $.get('<?=base_url()?>surat/disposisiDirekturGetDataDetil/'+smKode, function(data){
      var obj = JSON.parse(data);
      $('#smKode').val(obj.smKode);
      $('#smNomorSurat').html(obj.smNomorSurat);
      $('#smTanggalSurat').html(obj.smTanggalSurat);
      $('#smPerihal').html(obj.smPerihal);
      $('#smTujuan').html(obj.smTujuan);
      $('#keterangan').html(obj.smDisposisiDirut);
      $('#smKeterangan').html(obj.smKeterangan);
      $('#instansiNama').html(obj.instansiNama);
      $('#smTanggalTerima').html(obj.smTanggalTerima);
      $('#file').html(obj.file);
    })
  }

	function getDataFile(reffKode)
	{
		$.get('<?=base_url()?>surat/fileUploaderGetDataTb/'+reffKode, function(data){
			$('#data_view2').html(data);
		})
	}
</script>