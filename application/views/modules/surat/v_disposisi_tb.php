<table class="table table-striped table-hovered" id="tableData1" style="font-size: 13px">
	<thead>
		<tr>
			<th>Kode Disposisi</th>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Tanggal Disposisi</th>
			<th>Perihal</th>
			<th>Tujuan Surat</th>
			<th>Pengirim Surat / Instansi</th>
			<th>Keterangan Disposisi</th>
			<th>Unit Terkait</th>
			<th>Status Disposisi</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($surats): ?>
			<?php foreach ($surats as $srt): ?>
				<tr>
					<td><?=$srt->dspKode?></td>
					<td><?=$srt->smNomorSurat?></td>
					<td><?=basic_date($srt->smTanggalSurat)?></td>
					<td><?=basic_date($srt->dspTanggalDisposisi)?></td>
					<td><?=$srt->smPerihal?></td>
					<td><?=$srt->smTujuan?></td>
					<td><?=$srt->instansiNama?></td>
					<td>
						<?php 
							$dsp_keterangan = $cntrl->disposisiGetDataKeterangan($srt->dspKode); 
							if($dsp_keterangan)
							{
								foreach ($dsp_keterangan as $dspket) {
									echo $dspket->ketDeskripsi.'; ';
								} 
							}
						?>	
					</td>
					<td>
						<?php 
							$dsp_unit = $cntrl->disposisiGetDataUnit($srt->dspKode); 
							if($dsp_unit)
							{
								foreach ($dsp_unit as $unit) {
									echo $unit->unitNama.'; ';
								} 
							}
						?>
					</td>
					<td><?=$srt->dspStatusDetil?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectDataDisposisi" id="<?=$srt->dspKode?>">
							<i class="fas fa-check"></i>
						</button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData1').dataTable({
		"order" : []
	});
</script>
