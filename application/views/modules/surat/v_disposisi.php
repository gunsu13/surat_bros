<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Surat</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form role="form" id="formInput">
                      <div class="form-group">
                        <label for="smKode">Kode Surat Masuk - <button type="button" class="btn btn-primary btn-sm" id="btnMdSuratMasuk">Cari Surat</button></label>
                        <input type="text" class="form-control" name="smKode" id="smKode">
                        <input type="hidden" class="form-control" name="dspKode" id="dspKode">
                      </div>
                      <div class="form-group">
                        <label for="smNomorSurat">Nomor Surat</label>
                        <input type="text" class="form-control" name="smNomorSurat" id="smNomorSurat">
                      </div>
                      <div class="form-group">
                        <label for="smDisposisiDirut">Keterangan Direktur</label>
                        <input type="text" class="form-control" name="smDisposisiDirut" id="smDisposisiDirut">
                      </div>
                      <div class="form-group">
                        <label for="dspKeterangan">Keterangan Disposisi</label>
                          <select class="select2 form-control" multiple="multiple" name="keterangan[]" id="dspKeterangan">
                            <?php foreach ($kets as $ket): ?>
                            	<option value="<?=$ket->ketID?>"><?=$ket->ketDeskripsi?></option>
                            <?php endforeach ?>
                          </select>
                      </div>
                      <div class="form-group">
                        <label for="dspTanggalDisposisi">Tanggal Disposisi</label>
                        <input type="text" class="form-control" name="dspTanggalDisposisi" id="dspTanggalDisposisi" value="<?=date('m/d/Y')?>">
                      </div>
                      <div class="form-group">
                        <label for="dspTanggalMinimalRespon">Tanggal Minimal Respon</label>
                        <input type="text" class="form-control" name="dspTanggalMinimalRespon" id="dspTanggalMinimalRespon" value="<?=date('m/d/Y')?>">
                      </div>
                      <div class="form-group">
                        <label for="unitID">Unit Tujuan</label>
                        <select class="select2 form-control" multiple="multiple" name="unitID[]" id="unitID" style="width: 100%;">

                          <?php foreach ($units as $unit): ?>
                          	<option value="<?=$unit->unitID?>"><?=$unit->unitNama?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="dspCatatan">Notes</label>
                        <textarea class="form-control" rows="3" name="dspCatatan" id="dspCatatan"></textarea>
                      </div>
                    </form>
                  </div>
                </div>
                <hr>
                <button type="button" class="btn btn-default" id="btnMdUnit">
                  List Disposisi
                </button>
                <div class="btn btn-group float-right">
                	
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
	                <button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- modal -->
    <div class="modal fade" id="mdInstansi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Disposisi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

  <!-- modal -->
    <div class="modal fade" id="mdSuratMasuk">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Surat Masuk</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view1">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	$('#dspTanggalDisposisi').datepicker({
      autoclose: true
    })
    $('#dspTanggalMinimalRespon').datepicker({
      autoclose: true
    })
</script>
<script type="text/javascript">
	var edit = false;

	$('#btnMdSuratMasuk').click(function(){
		getDataTabelSuratMasuk();
		$('#mdSuratMasuk').modal('show');
	})

	$('body').on('click', '.btnSelectData', function(){
		var suratKode = $(this).attr('id');
		var data = getDataSinggleSuratMasuk(suratKode);

		var obj = JSON.parse(data);

		$('#smKode').val(obj.smKode);
		$('#smNomorSurat').val(obj.smNomorSurat);
		$('#smDisposisiDirut').val(obj.smDisposisiDirut);
		$('#smKode').attr('readonly', true);
		$('#smNomorSurat').attr('readonly', true);
		$('#smDisposisiDirut').attr('readonly', true);
		edit = false;
		$('#mdSuratMasuk').modal('hide');
	})

	$('body').on('click', '.btnSelectDataDisposisi', function(){
		var suratKode = $(this).attr('id');
		var data = getDataSinggle(suratKode);

		var obj = JSON.parse(data);

		$('#smKode').val(obj.smKode);
		$('#smNomorSurat').val(obj.smNomorSurat);
		$('#smPerihal').val(obj.smPerihal);
		$('#smTanggalSurat').val(obj.dateConverted_tanggalSurat);
		$('#smTanggalTerima').val(obj.dateConverted_tanggalTerima);
		$('#smTujuan').val(obj.smTujuan);
		$('#instansiKode').val(obj.kodePengirim);
		$('#smKeterangan').val(obj.smKeterangan);

		edit = true;

		$('#mdInstansi').modal('hide');
	})

	$('#btnHapus').click(function(){
		var instansiKode = $('#instansiKode').val();

		if (instansiKode == '') 
		{
			$("#formInput")[0].reset();
		}else{
			Swal.fire({
			  title: 'Apakah anda yakin?',
			  text: "Data yang dihapus akan hilang dari list",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Hapus !'
			}).then((result) => {
			  if (result.value) {

			  	$.ajax({
			        url: '<?=base_url()?>master/instansiDelete/'+instansiKode,
			        type: 'GET',
			        dataType: 'html',
	        		async: false
			    }) 
			    .done(function(data) {
			    	console.log(data);
			     	var obj = JSON.parse(data);

			     	if (obj.status == 'true') 
			     	{
			     		edit = false;
			     		$("#formInput")[0].reset();

			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}else{
			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}
			    })
			  	.fail(function (jqXHR, textStatus, error) {
			      	console.log("Post error: " + error);
			  	});

			    
			  }
			})
		}
	})


	$('#btnMdUnit').click(function(){
		getDataTabel();
		$('#mdInstansi').modal('show');
	})

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInput').serialize();

		if (edit == false) 
		{
			$.ajax({
		        url: '<?=base_url()?>surat/disposisiCreate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize,
				beforeSend: function(){
					$('#btnSimpan').prop('disabled', true);
				},
				complete: function(){
					$('#btnSimpan').prop('disabled', false);
				}
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		$('#smKode').attr('readonly', false);
		     		$('#smNomorSurat').attr('readonly', false);
		     		$("#unitID").val(null).trigger('change');
		     		$('#keterangan').val(null).trigger('change');

		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}else{
			$.ajax({
		        url: '<?=base_url()?>surat/disposisiUpdate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		$('#smKode').attr('readonly', false);
		     		$('#smNomorSurat').attr('readonly', false);
		     		$("#unitID").val(null).trigger('change');
		     		$('#keterangan').val(null).trigger('change');

		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
					
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}
	})

	function getDataTabel()
	{
		$.get('<?=base_url()?>surat/disposisiGetDataTabel', function(data){
			$('#data_view').html(data);
		})
	}

	function getDataTabelSuratMasuk()
	{
		$.get('<?=base_url()?>surat/disposisiSuratMasukGetDataTabel', function(data){
			$('#data_view1').html(data);
		})
	}

	function getDataSinggle(dspKode)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>surat/disposisiGetDataSinggle/'+dspKode,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}

	function getDataSinggleSuratMasuk(suratKode)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>surat/suratMasukGetDataSinggle/'+suratKode,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}

</script>
