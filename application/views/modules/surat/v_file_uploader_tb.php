<table class="table" style="font-size: 13px;">
	<thead>
		<tr>
			<th>File Kode</th>
			<th>File Name</th>
			<th>File Size</th>
			<th>File Type</th>
			<th>View</th>
			<th>Download</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($files): ?>
			<?php foreach ($files as $fl): ?>
				<tr>
					<td><?=$fl->fileKode?></td>
					<td><?=$fl->fileName?></td>
					<td><?=$fl->fileSize?></td>
					<td><?=$fl->fileType?></td>
					<td>
						<?php if ($fl->fileType == 'application/pdf'): ?>
							<a href="#" class="btnOpenViewer" id="<?=$fl->fileKode?>">View</a>
						<?php else: ?>
							cant view file is not pdf
						<?php endif ?>
						<!-- <a href="#" class="btnOpenViewer" id="<?=$fl->fileDirectory?>">View</a> -->
					</td>
					<td><a href="<?=base_url()?>surat/fileDownloader/<?=$fl->fileName?>">Download</a></td>
					<td><a href="#" class="btnRemoveFile" id="<?=$fl->fileKode?>">Hapus</a></td>
				</tr>
			<?php endforeach ?>
		<?php else: ?>
			<tr>
				<td colspan="5">Tidak ada data file ..</td>
			</tr>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('.btnOpenViewer').click(function(){
		var fileKode = $(this).attr('id');

		window.open('<?=base_url()?>surat/filePdfViewer/'+fileKode, 
                         'newwindow', 
                         'width=800,height=842'); 
              return false;
	})
</script>