<table class="table table-striped table-hovered" id="tableData" style="font-size: 13px;">
	<thead>
		<tr>
			<th>Kode Surat</th>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Perihal</th>
			<th>Tujuan Surat</th>
			<th>Penerima Surat / Instansi</th>
			<th>Notes</th>
			<th>Status Surat</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($surats): ?>
			<?php foreach ($surats as $srt): ?>
				<tr>
					<td><?=$srt->skKode?></td>
					<td><?=$srt->skNomorSurat?></td>
					<td><?=basic_date($srt->skTanggalKeluar)?></td>
					<td><?=$srt->skPerihal?></td>
					<td><?=$srt->skTujuan?></td>
					<td><?=$srt->instansiNama?></td>
					<td><?=$srt->skKeterangan?> <br> <a href="#" class="btnFileView" id="<?=$srt->skKode?>">File ( <?php $jml_file = $cntrl->suratMasukGetDataCountFile($srt->skKode); echo $jml_file; ?> )</a></td>
					<td><?=$srt->skStatusDetil?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectData" id="<?=$srt->skKode?>">
							<i class="fas fa-check"></i>
						</button>
						<a class="btn btn-default btn-sm" href="<?=base_url()?>surat/fileUploader/<?=$srt->skKode?>">Upload File</a>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>