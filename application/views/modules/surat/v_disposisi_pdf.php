<?php

// create new PDF document
$pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Sistem Informasi Manajemen Surat BROS');
$pdf->SetAuthor('User : ');
$pdf->SetTitle('Laporan Disposisi');
$pdf->SetSubject('Laporan disposisi');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// -----------------------------------------------------------------------------
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// set font
$pdf->SetFont('helvetica', '', 11);

// add a page
$pdf->AddPage();

// // set some text to print
// $txt = <<<EOD


// LEMBAR DISPOSISI
// EOD;

// // print a block of text using Write()
// $pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

$html3 = '<table border="1" cellpadding="10">
	<tr style="text-align:center;">
		<td colspan="4">DISPOSISI SURAT MASUK</td>
	</tr>
	<tr>
      <td>Nomor Surat</td>
      <td>'.$disposisi->smNomorSurat.'</td>
      <td>Tanggal Disposisi</td>
      <td>'.$disposisi->dspTanggalDisposisi.'</td>
    </tr>
     <tr>
      <td>Tanggal Surat</td>
      <td>'.$disposisi->smTanggalSurat.'</td>
      <td>Keterangan</td>
      <td>'.$keterangan.'</td>
    </tr>
     <tr>
      <td>Perihal</td>
      <td>'.$disposisi->smPerihal.'</td>
      <td>Catatan Disposisi</td>
      <td>'.$disposisi->dspCatatan.'</td>
    </tr>
     <tr>
      <td>Tujuan</td>
      <td>'.$disposisi->smTujuan.'</td>
      <td>Instansi Pengirim</td>
      <td>'.$disposisi->instansiNama.'</td>
    </tr>
</table>';


$pdf->writeHTMLCell($w=190, $h=2, $x='9', $y='40', $html3, $border=0, $ln=1, $fill=0, $reseth=false, $align='L', $autopadding=true);

//Close and output PDF document
$pdf->Output('example_003.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+