<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Surat</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form role="form" id="formInput">
                      <div class="form-group">
                        <label for="smKode">Kode Surat Masuk - <button type="button" class="btn btn-primary btn-sm" id="btnMdSuratMasuk">Cari Surat</button></label>
                        <input type="text" class="form-control" name="smKode" id="smKode">
                        <input type="hidden" class="form-control" name="dspKode" id="dspKode">
                      </div>
                      <div class="form-group">
                        <label for="smNomorSurat">Nomor Surat</label>
                        <input type="text" class="form-control" name="smNomorSurat" id="smNomorSurat">
                      </div>
                      <div class="form-group">
                        <label for="dspCatatan">Arsip Notes</label>
                        <textarea class="form-control" rows="3" name="dspCatatan" id="dspCatatan"></textarea>
                      </div>
                    </form>
                  </div>
                </div>
                <hr>
                <button type="button" class="btn btn-default" id="btnArsipSurat">
                  List Arsip
                </button>
                <div class="btn btn-group float-right">
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
	                <button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- modal -->
    <div class="modal fade" id="mdInstansi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Disposisi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

  <!-- modal -->
    <div class="modal fade" id="mdSuratMasuk">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Arsip Surat</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view1">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

  <!-- modal -->
  <div class="modal fade" id="mdFile">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">File Surat</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view2">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	$('#dspTanggalDisposisi').datepicker({
      autoclose: true
    })
    $('#dspTanggalMinimalRespon').datepicker({
      autoclose: true
    })
</script>
<script type="text/javascript">
	var edit = false;

	$('body').on('click', '.btnFileView', function(){
		var reffKode = $(this).attr('id');
		getDataFile(reffKode);

		$('#mdFile').modal('show');
	})
	
	function getDataFile(reffKode)
	{
		$.get('<?=base_url()?>surat/fileUploaderGetDataTb/'+reffKode, function(data){
			$('#data_view2').html(data);
		})
	}

	$('body').on('click', '.btnReOpen', function(){
		var smKode = $(this).attr('id');

		Swal.fire({
		  title: 'Apakah anda yakin?',
		  text: "Arsip akan di re-open",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, Re Open !'
		}).then((result) => {
		  if (result.value) {

		  	$.get('<?=base_url()?>surat/arsipSuratMasukReOpen/'+smKode, function(data){
				var obj = JSON.parse(data);

				if (obj.status == 'true') 
				{
					getDataArsip();
				}else{
			 		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
				}
			})		  
		  }
		})
	})

	$('#btnArsipSurat').click(function(){
		getDataArsip();
		$('#mdSuratMasuk').modal('show');
	});

	$('#btnMdSuratMasuk').click(function(){
		getDataTabelSuratMasuk();
		$('#mdSuratMasuk').modal('show');
	})

	$('body').on('click', '.btnSelectData', function(){
		var suratKode = $(this).attr('id');
		var data = getDataSinggleSuratMasuk(suratKode);

		var obj = JSON.parse(data);

		$('#smKode').val(obj.smKode);
		$('#smNomorSurat').val(obj.smNomorSurat);
		$('#smKode').attr('readonly', true);
		$('#smNomorSurat').attr('readonly', true);
		edit = false;
		$('#mdSuratMasuk').modal('hide');
	})

	$('body').on('click', '.btnSelectDataDisposisi', function(){
		var suratKode = $(this).attr('id');
		var data = getDataSinggle(suratKode);

		var obj = JSON.parse(data);

		$('#smKode').val(obj.smKode);
		$('#smNomorSurat').val(obj.smNomorSurat);
		$('#smPerihal').val(obj.smPerihal);
		$('#smTanggalSurat').val(obj.dateConverted_tanggalSurat);
		$('#smTanggalTerima').val(obj.dateConverted_tanggalTerima);
		$('#smTujuan').val(obj.smTujuan);
		$('#instansiKode').val(obj.kodePengirim);
		$('#smKeterangan').val(obj.smKeterangan);

		edit = true;

		$('#mdInstansi').modal('hide');
	})

	$('#btnSimpan').click(function(){
		var smKode = $('#smKode').val();
		$.get('<?=base_url()?>surat/arsipSuratMasukValidasiDisposisi/'+smKode, function(data) {
			var validasi = JSON.parse(data);
			console.log(validasi);

			if (validasi.status != 'false') {	
				Swal.fire({
					title: 'Apakah anda yakin?',
					text: "data surat akan diarsipkan",
					icon: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Arsipkan !'
				}).then((result) => {
					if (result.value) {
						simpanArsip();
					}
				})		
			}else{
				Swal.fire(
				'Info',
				validasi.message,
				'warning'
				);
			}
	    });
	})

	function simpanArsip()
	{
		
		var dataSerialize = $('#formInput').serialize();
		$.ajax({
			url: '<?=base_url()?>surat/arsipSuratMasukSave',
			type: 'POST',
			dataType: 'text',
			data: dataSerialize
		}) 
		.done(function(data) {
			console.log(data);
			var obj = JSON.parse(data);

			if (obj.status == 'true') 
			{
				edit = false;
				$("#formInput")[0].reset();
				$('#smKode').attr('readonly', false);
				$('#smNomorSurat').attr('readonly', false);

				Swal.fire(
					'Info',
					obj.message,
					'success'
				);
			}else{
				Swal.fire(
					'Info',
					obj.message,
					'warning'
				);
			}
		})
		.fail(function (jqXHR, textStatus, error) {
			console.log("Post error: " + error);
		});
	}

	function getDataTabel()
	{
		$.get('<?=base_url()?>surat/disposisiGetDataTabel', function(data){
			$('#data_view').html(data);
		})
	}

	function getDataTabelSuratMasuk()
	{
		$.get('<?=base_url()?>surat/suratMasukGetDataTabel', function(data){
			$('#data_view1').html(data);
		})
	}

	function getDataArsip()
	{
		$.get('<?=base_url()?>surat/arsipSuratMasukGetDataTb', function(data){
			$('#data_view1').html(data);
		})
	}

	function validasiPengarsipan(smKode)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>surat/arsipSuratMasukValidasiDisposisi/'+smKode,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}

	function getDataSinggleSuratMasuk(suratKode)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>surat/suratMasukGetDataSinggle/'+suratKode,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}

</script>
