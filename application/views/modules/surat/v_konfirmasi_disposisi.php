<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Surat</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-3">
                    <form id="formFilter">
                      <div class="form-group">
                        <label>Unit</label>
                        <select class="form-control" id="unitID" name="unitID">
                          <option value="-">- Pilih Unit -</option>
                          <?php if ($this->session->userdata('level') == 1): ?>                          
                            <?php foreach ($units as $unit): ?>
                              <option value="<?=$unit->unitID?>"><?=$unit->unitNama?></option>
                            <?php endforeach ?>
                          <?php else: ?>
                            <?php foreach ($units as $unit): ?>
                              <?php if ($unit->unitID == $this->session->userdata('unitID')): ?>
                                <option value="<?=$unit->unitID?>"><?=$unit->unitNama?></option>
                              <?php endif ?>
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                  	<div id="data_view">
                  		
                  	</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal -->
    <div class="modal fade" id="mdInstansi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Konfirmasi Disposisi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<form id="formKonfirmasiDisposisi">
                  <div class="row">
                    <div class="col-md-12">
                      <table class="table" style="font-size: 13px;">
                        <tr>
                          <td width="15%">Nomor Surat</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smNomorSurat"></td>
                          <td width="15%">Tanggal Disposisi</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="dspTanggalDisposisi"></td>
                        </tr>
                         <tr>
                          <td width="15%">Tanggal Surat</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTanggalSurat"></td>
                          <td width="15%">Keterangan</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="dspKeterangan"></td>
                        </tr>
                         <tr>
                          <td width="15%">Perihal</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smPerihal"></td>
                          <td width="15%">Catatan Disposisi</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="dspCatatan"></td>
                        </tr>
                        
                         <tr>
                          <td width="15%">Tujuan</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTujuan"></td>
                          <td width="15%">Instansi Pengirim</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="instansiNama"></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <label>Keterangan Konfirmasi</label>
                      <input type="hidden" name="id" id="dspUnitID">
                      <textarea name="keteranganKonfirmasi" class="form-control" id="keteranganKonfirmasi"></textarea>
                    </div>
                  </div>  
                </form>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button type="button" class="btn btn-primary" id="btnKonfirmasiDisposisiFinal">Konfirmasi</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

  <!-- modal -->
    <div class="modal fade" id="mdTransfer">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Transfer Disposisi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <form id="formTransferDisposisi">
                  <div class="row">
                    <div class="col-md-12">
                      <table class="table" style="font-size: 13px;">
                        <tr>
                          <td width="15%">Nomor Surat</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smNomorSurat2"></td>
                          <td width="15%">Tanggal Disposisi</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="dspTanggalDisposisi2"></td>
                        </tr>
                         <tr>
                          <td width="15%">Tanggal Surat</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTanggalSurat2"></td>
                          <td width="15%">Keterangan</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="dspKeterangan2"></td>
                        </tr>
                         <tr>
                          <td width="15%">Perihal</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smPerihal2"></td>
                          <td width="15%">Catatan Disposisi</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="dspCatatan2"></td>
                        </tr>
                        
                         <tr>
                          <td width="15%">Tujuan</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="smTujuan2">/td>
                          <td width="15%">Instansi Pengirim</td>
                          <td width="2.5%">:</td>
                          <td width="32.5%" id="instansiNama2"></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Keterangan Transfer</label>
                        <input type="hidden" name="id" id="dspUnitID2">
                        <input type="hidden" name="fromUnitID" id="fromUnitID" value="<?=$this->session->userdata('unitID')?>">
                        <textarea name="keteranganKonfirmasi" class="form-control" id="keteranganKonfirmasi2"></textarea>
                      </div>
                    </div>
                  </div>  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Unit Yang Dituju</label>
                        <select class="form-control" id="toUnitID" name="toUnitID">
                          <option value="-">- Pilih Unit -</option>
                          <?php foreach ($units as $unit): ?>
                            <option value="<?=$unit->unitID?>"><?=$unit->unitNama?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                  </div>  
                </form>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button type="button" class="btn btn-primary" id="btnTransferDisposisiFinal">Konfirmasi</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

	<!-- modal -->
    <div class="modal fade" id="mdFile">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">File Surat</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view2">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

<?php $this->load->view('modules/layouts/v_layout_footer'); ?>

<script type="text/javascript">
  var unitID = $('#unitID').val();

	getDataKonfirmasi(unitID);
  
  $('#btnKonfirmasiDisposisiFinal').click(function(){
    var dataSerialize = $('#formKonfirmasiDisposisi').serialize();

    $.ajax({
        url: '<?=base_url()?>surat/disposisiKonfirmasiCreate',
        type: 'POST',
        dataType: 'text',
        data: dataSerialize
    }) 
    .done(function(data) {
      console.log(data);
      var obj = JSON.parse(data);

      if (obj.status == 'true') 
      {
        edit = false;
        $("#formKonfirmasiDisposisi")[0].reset();
        $('#mdInstansi').modal('hide');
        getDataKonfirmasi(unitID);
        Swal.fire(
          'Info',
          obj.message,
          'success'
        );
      }else{
        Swal.fire(
          'Info',
          obj.message,
          'warning'
        );
      }
    })
    .fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
  })

  $('#btnTransferDisposisiFinal').click(function(){
    var dataSerialize = $('#formTransferDisposisi').serialize();

    $.ajax({
        url: '<?=base_url()?>surat/disposisiTransfer',
        type: 'POST',
        dataType: 'text',
        data: dataSerialize
    }) 
    .done(function(data) {
      console.log(data);
      var obj = JSON.parse(data);

      if (obj.status == 'true') 
      {
        edit = false;
        $("#formTransferDisposisi")[0].reset();
        $('#mdTransfer').modal('hide');
        getDataKonfirmasi(unitID);
        Swal.fire(
          'Info',
          obj.message,
          'success'
        );
      }else{
        Swal.fire(
          'Info',
          obj.message,
          'warning'
        );
      }
    })
    .fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
  })

  $("#unitID").change(function(){
    unitID = $(this).val();
    getDataKonfirmasi(unitID);
  });
	
  $('body').on('click', '.btnKonfirmasiDisposisi', function(){
    var dspUnitID = $(this).attr('id');
		console.log(dspUnitID);
    getDataKonfirmasiDetil(dspUnitID);
    $('#mdInstansi').modal('show');
  })

  $('body').on('click', '.btnTransferDisposisi', function(){
    var dspUnitID = $(this).attr('id');
    getDataTransferDetil(dspUnitID);
    $('#mdTransfer').modal('show');
  })

  $('body').on('click', '.btnTerimaDisposisi', function(){
    var dspUnitID = $(this).attr('id');
    var exce = updateTerimaSurat(dspUnitID);
    if (exce.status != 'true') 
    {
      getDataKonfirmasi(unitID);
    }else{
      Swal.fire(
        'Info',
        'failed to update the data',
        'warning'
      );
    }
  })

  $('body').on('click', '.btnFileView', function(){
		var reffKode = $(this).attr('id');
		getDataFile(reffKode);

		$('#mdFile').modal('show');
	})

	function getDataKonfirmasi(unitID)
	{
		$.get('<?=base_url()?>surat/disposisiKonfirmasiGetData/'+unitID, function(data){
			$('#data_view').html(data);
		})
	}

  function getDataKonfirmasiDetil(dspUnitID)
  {
    $.get('<?=base_url()?>surat/disposisiKonfirmasiGetDataDetil/'+dspUnitID, function(data){
      var obj = JSON.parse(data);
      $('#dspUnitID').val(obj.dspUnitID);
      $('#smNomorSurat').html(obj.smNomorSurat);
      $('#smTanggalSurat').html(obj.smTanggalSurat);
      $('#smPerihal').html(obj.smPerihal);
      $('#smTujuan').html(obj.smTujuan);
      $('#dspKeterangan').html(obj.keteranganDisposisi);
      $('#dspTanggalDisposisi').html(obj.dspTanggalDisposisi);
      $('#dspCatatan').html(obj.dspCatatan);
      $('#instansiNama').html(obj.instansiNama);
    })
  }

  function getDataTransferDetil(dspUnitID)
  {
    $.get('<?=base_url()?>surat/disposisiKonfirmasiGetDataDetil/'+dspUnitID, function(data){
      var obj = JSON.parse(data);
      $('#dspUnitID2').val(obj.dspUnitID);
      $('#smNomorSurat2').html(obj.smNomorSurat);
      $('#smTanggalSurat2').html(obj.smTanggalSurat);
      $('#smPerihal2').html(obj.smPerihal);
      $('#smTujuan2').html(obj.smTujuan);
      $('#dspKeterangan2').html(obj.keteranganDisposisi);
      $('#dspTanggalDisposisi2').html(obj.dspTanggalDisposisi);
      $('#dspCatatan2').html(obj.dspCatatan);
      $('#instansiNama2').html(obj.instansiNama);
    })
  }

	function getDataFile(reffKode)
	{
		$.get('<?=base_url()?>surat/fileUploaderGetDataTb/'+reffKode, function(data){
			$('#data_view2').html(data);
		})
	}

  function updateTerimaSurat(dspUnitID)
  {
    var result = null;

    $.ajax({
          url: '<?=base_url()?>surat/disposisiUpdateTerimaSurat/'+dspUnitID,
          type: 'GET',
          dataType: 'html',
          async: false,
          success: function(data) {
              result = data;
          } 
       });

    return result;
  }
</script>
