<table class="table table-striped table-hover" id="tableData" style="font-size: 13px;">
	<thead>
		<tr>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Perihal</th>
			<th>Tujuan</th>
			<th>Catatan Surat</th>
			<th>Keterangan Disposisi</th>
			<th>Catatan Disposisi</th>
			<th>File</th>
			<th>Konfirmasi</th>
			<th>Transfer</th>
			<th>Print</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($disps): ?>
			<?php foreach ($disps as $dsp): ?>
				<tr>
					<td><?=$dsp->smNomorSurat?></td>
					<td><?=basic_date($dsp->smTanggalSurat)?></td>
					<td><?=$dsp->smPerihal?></td>
					<td><?=$dsp->smTujuan?></td>
					<td><?=$dsp->smKeterangan?></td>
					<td><?php 
							$dsp_keterangan = $cntrl->disposisiGetDataKeterangan($dsp->dspKode); 
							if($dsp_keterangan){
								foreach ($dsp_keterangan as $dspket) {
									echo $dspket->ketDeskripsi.'; ';
								} 
							}
						?>	</td>
					<td><?=$dsp->dspCatatan?></td>
					<td>
						<a href="#" class="btnFileView" id="<?=$dsp->smKode?>">File ( <?php $jml_file = $cntrl->suratMasukGetDataCountFile($dsp->smKode); echo $jml_file; ?> )</a>
					</td>
					<td>
						<?php if ($dsp->tanggalTerima == null): ?>
							<a href="#" class="btn btn-success btn-sm btnTerimaDisposisi" id="<?=$dsp->dspUnitID?>">Terima</a>
						<?php endif ?>	
						<?php if ($dsp->tanggalTerima != null and $dsp->tanggalKonfirmasi == null): ?>
							<a href="#" class="btn btn-primary btn-sm btnKonfirmasiDisposisi" id="<?=$dsp->dspUnitID?>">Konfirmasi</a> <br>
						<?php endif ?>
					</td>
					<td>
						<?php if ($dsp->tanggalTerima != null and $dsp->tanggalKonfirmasi == null): ?>
							<a href="#" class="btn btn-success btn-sm btnTransferDisposisi" id="<?=$dsp->dspUnitID?>">Transfer</a><br>
						<?php endif ?>
					</td>
					<td>
						<a href="#" class="btn btn-danger btn-sm btnPrintDsp" id="<?=$dsp->dspUnitID?>">Print</a>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable({
		"ordering": false
	});
	$('.btnPrintDsp').click(function(){
		var dspKode = $(this).attr('id');
		window.open('<?=base_url()?>surat/disposisiPrint/'+dspKode, 'newwindow', 'width=800,height=842'); 
        return false;
	})
</script>
