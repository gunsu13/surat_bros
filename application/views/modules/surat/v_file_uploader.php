<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Surat</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <?php if ($jenis_surat == 'SM'): ?>
                        <a href="<?=base_url()?>surat/suratMasuk" class="btn btn-default">Kembali</a>
                        <hr>
                        <h4>Upload File Scan atau PDF Surat</h4>
                        <table class="table">
                            <tr>
                                <td width="15%">Kode Surat</td>
                                <td width="5%">:</td>
                                <td width="30%"><?=$surat->smKode?></td>
                                <td width="15%">Nomor Surat</td>
                                <td width="5%">:</td>
                                <td width="30%"><?=$surat->smNomorSurat?></td>
                            </tr>
                            <tr>
                                <td>Perihal</td>
                                <td>:</td>
                                <td><?=$surat->smPerihal?></td>
                                <td>Tujuan</td>
                                <td>:</td>
                                <td><?=$surat->smTujuan?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Surat</td>
                                <td>:</td>
                                <td><?=$surat->smTanggalSurat?></td>
                                <td>Tanggal Terima</td>
                                <td>:</td>
                                <td><?=$surat->smTanggalTerima?></td>
                            </tr>
                            <tr>
                                <td>Instansi Pengirim</td>
                                <td>:</td>
                                <td><?=$surat->instansiNama?> <br> ( <?=$surat->instansiAlamat?> / <?=$surat->instansiNomorTlp?> / <?=$surat->instansiEmail?> )</td>
                                <td>Notes</td>
                                <td>:</td>
                                <td><?=$surat->smKeterangan?></td>
                            </tr>
                        </table>
                    <?php endif ?>
                    <?php if ($jenis_surat == 'SK'): ?>
                        <a href="<?=base_url()?>surat/suratKeluar" class="btn btn-default">Kembali</a>
                        <hr>
                        <h4>Upload File Scan atau PDF Surat</h4>
                        <table class="table">
                            <tr>
                                <td width="15%">Kode Surat</td>
                                <td width="5%">:</td>
                                <td width="30%"><?=$surat->skKode?></td>
                                <td width="15%">Nomor Surat</td>
                                <td width="5%">:</td>
                                <td width="30%"><?=$surat->skNomorSurat?></td>
                            </tr>
                            <tr>
                                <td>Perihal</td>
                                <td>:</td>
                                <td><?=$surat->skPerihal?></td>
                                <td>Tujuan</td>
                                <td>:</td>
                                <td><?=$surat->skTujuan?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Surat</td>
                                <td>:</td>
                                <td><?=$surat->skTanggalKeluar?></td>
                                <td>Instansi Tujuan / Penerima</td>
                                <td>:</td>
                                <td><?=$surat->instansiNama?> <br> ( <?=$surat->instansiAlamat?> / <?=$surat->instansiNomorTlp?> / <?=$surat->instansiEmail?> )</td>
                            </tr>
                            <tr>
                                <td>Notes</td>
                                <td>:</td>
                                <td><?=$surat->skKeterangan?></td>
                            </tr>
                        </table>
                    <?php endif ?>
                  </div>
                </div>
                <hr>
                <div class="row">
		            <form enctype="multipart/form-data" id="formUpload">
	                	<div class="col-md-12">        		
		                	<div class="form-group">
		                		<label>Upload File* ( .pdf is recomended )</label>
		                		<input type="hidden" name="reffKode" value="<?=$reffKode?>">
		                		<input type="hidden" name="jenis_surat" value="<?=$jenis_surat?>">
		                		<input type="file" name="files[]" class="form-control" id="file" multiple>
		                	</div>
		                	<div class="form-group">
		                		<button type="submit" class="btn btn-primary">Simpan</button>
		                	</div>
	                	</div>
                	</form>	
                </div>
                <hr>
                <div class="row">
                	<div class="col-md-12">

            			<h4>File List</h4>
                		<div id="data_view">
                			
                		</div>
                	</div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	var edit = false;
	var reffKode = '<?=$reffKode?>';

	getDataFile(reffKode);
	
	$('#formUpload').on('submit', function(e){
		e.preventDefault();
		
		$.ajax({
            url: '<?=base_url()?>surat/fileUploaderSave',
            type: 'POST',
            contentType: false,
            cache: false,
            processData:false,
            data:  new FormData(this),
            beforeSend: function() {  
              $('#loader').show();            
          }

      }) 
        .done(function(data) {

            var obj = JSON.parse(data);

            if (obj.status == 'true') {

            	location.reload();
            }else{
            	Swal.fire(
                  'Info',
                  obj.message,
                  'warning'
                  );
            }
        })
        .fail(function (jqXHR, textStatus, error) {
         console.log("Post error: " + error);
     });
    })

    $('body').on('click', '.btnRemoveFile', function(){
        var fileKode = $(this).attr('id');

        Swal.fire({
            title: 'Are you sure?',
            text: "File in directory will be remove!",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                $.get('<?=base_url()?>surat/fileRemove/'+fileKode, function(data){
                    var obj = JSON.parse(data);
                    console.log(obj);
                    if (obj.status == 'true') 
                    {
                        Swal.fire(
                          'Info',
                          obj.message,
                          'info'
                          );
                        getDataFile(reffKode);
                    }else{
                        Swal.fire(
                          'Info',
                          obj.message,
                          'warning'
                          );
                    }
                })
            }
        })
    })

    function getDataFile(reffKode)
    {
      $.get('<?=base_url()?>surat/fileUploaderGetDataTb/'+reffKode, function(data){
         $('#data_view').html(data);
     })
  }
</script>