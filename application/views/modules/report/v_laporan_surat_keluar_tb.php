<table class="table table-striped table-hovered" id="tableData" style="font-size: 13px;">
	<thead>
		<tr>
			<th>Kode Surat</th>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Perihal</th>
			<th>Tujuan Surat</th>
			<th>Penerima Surat / Instansi</th>
			<th>Notes</th>
			<th>Status Surat</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($surats): ?>
			<?php foreach ($surats as $srt): ?>
				<tr>
					<td><?=$srt->skKode?></td>
					<td><?=$srt->skNomorSurat?></td>
					<td><?=basic_date($srt->skTanggalKeluar)?></td>
					<td><?=$srt->skPerihal?></td>
					<td><?=$srt->skTujuan?></td>
					<td><?=$srt->instansiNama?></td>
					<td><?=$srt->skKeterangan?></td>
					<td><?=$srt->skStatusDetil?></td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>