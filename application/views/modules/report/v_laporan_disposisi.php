<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <form id="formFilter">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Status Disposisi</label>
                        <select class="form-control" id="statusDsp" name="statusDsp">
                          <option value="">- Pilih Status -</option>
                          <option value="1">Sudah Selesai</option>
                          <option value="0">Belum Selesai</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Periode Tanggal Surat</label>
                        <input type="text" name="dateFiltered" id="dateFiltered" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>-</label><br>
                        <button type="button" id="btnFilter" class="btn btn-primary">Filter</button>
                        <button type="submit" id="btnExport" class="btn btn-success">Excel</button>
                      </div>
                    </div>
                  </div>
                </form>
                <div class="row">
                  <div class="col-md-12">
                  	<div id="data_view">
                  		
                  	</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('modules/layouts/v_layout_footer'); ?>

<script type="text/javascript">
  var dspStatus = $('#statusDsp').val();
  var dateFiltered = $('#dateFiltered').val();

	getDataSuratDisposisi(dspStatus, dateFiltered);

  $('#btnFilter').click(function(){
    dspStatus = $('#statusDsp').val();
    instansiKode = $('#instansiKode').val();
    dateFiltered = $('#dateFiltered').val();
    getDataSuratDisposisi(dspStatus, dateFiltered);
  })

	function getDataSuratDisposisi(dspStatus, dateFiltered)
	{
		$.ajax({
        url: '<?=base_url()?>report/disposisi_getDataTb',
        type: 'POST',
        dataType: 'text',
        data: 'dspStatus='+dspStatus+'&dateFiltered='+dateFiltered
    }) 
    .done(function(data) {
      $('#data_view').html(data);
    })
    .fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
	}

</script>