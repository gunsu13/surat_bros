<table class="table table-striped table-hovered" id="tableData" style="font-size: 13px;">
	<thead>
		<tr>
			<th>Kode Surat</th>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Tanggal Terima</th>
			<th>Tujuan Surat</th>
			<th>Asal Instansi</th>
			<th>Perihal</th>
			<th>Tanggal Arsip</th>
			<th>Keterangan Arsip</th>
			<th>File</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($arsips): ?>
			<?php foreach ($arsips as $srt): ?>
				<tr>
					<td><?=$srt->smKode?></td>
					<td><?=$srt->smNomorSurat?></td>
					<td><?=basic_date($srt->smTanggalSurat)?></td>
					<td><?=basic_date($srt->smTanggalTerima)?></td>
					<td><?=$srt->smTujuan?></td>
					<td><?=$srt->instansiNama?></td>
					<td><?=$srt->smPerihal?></td>
					<td><?=basic_date($srt->smTanggalArsip)?></td>
					<td><?=$srt->smKeteranganArsip?></td>
					<td><a href="#" class="btnFileView" id="<?=$srt->smKode?>">File ( <?php $jml_file = getCountFile($srt->smKode); echo $jml_file; ?> )</a></td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>