<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <form id="formFilter">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Instansi ( Asal Surat )</label>
                        <select class="form-control" id="instansiKode" name="instansiKode">
                          <option value="">- Pilih Instansi -</option>
                          <?php foreach ($instansis as $ins): ?>
                            <option value="<?=$ins->instansiKode?>"><?=$ins->instansiNama?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Periode Tanggal Terima</label>
                        <input type="text" name="dateFiltered" id="dateFiltered" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>-</label><br>
                        <button type="button" id="btnFilter" class="btn btn-primary">Filter</button>
                        <button type="submit" id="btnExport" class="btn btn-success">Excel</button>
                      </div>
                    </div>
                  </div>
                </form>
                <div class="row">
                  <div class="col-md-12">
                  	<div id="data_view">
                  		
                  	</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

    <!-- modal -->
    <div class="modal fade" id="mdFile">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">File Surat</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div id="data_view2">
                  
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->

<?php $this->load->view('modules/layouts/v_layout_footer'); ?>

<script type="text/javascript">
  var instansiKode = $('#instansiKode').val();
  var dateFiltered = $('#dateFiltered').val();

	getDataArsipSurat(instansiKode, dateFiltered);

  $('#btnFilter').click(function(){
    instansiKode = $('#instansiKode').val();
    dateFiltered = $('#dateFiltered').val();

    getDataArsipSurat(instansiKode, dateFiltered);
  })

  $('body').on('click', '.btnFileView', function(){
    var reffKode = $(this).attr('id');
    getDataFile(reffKode);

    $('#mdFile').modal('show');
  })

  function getDataFile(reffKode)
  {
    $.get('<?=base_url()?>surat/fileUploaderGetDataTb/'+reffKode, function(data){
      $('#data_view2').html(data);
    })
  }

	function getDataArsipSurat(instansiKode, dateFiltered)
	{
		$.ajax({
        url: '<?=base_url()?>report/arsip_surat_masuk_tb',
        type: 'POST',
        dataType: 'text',
        data: 'instansiKode='+instansiKode+'&dateFiltered='+dateFiltered
    }) 
    .done(function(data) {
      $('#data_view').html(data);
    })
    .fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
	}

</script>