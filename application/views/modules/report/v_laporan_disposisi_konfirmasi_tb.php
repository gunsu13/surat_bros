<div class="table-responsive">
  <table class="table table-striped table-hovered" id="tableData" style="font-size: 13px">
    <thead>
      <tr>
        <th>Kode Disposisi</th>
        <th>Nomor Surat</th>
        <th>Tanggal Surat</th>
        <th>Tanggal Disposisi</th>
        <th>Perihal</th>
        <th>Tujuan Surat</th>
        <th>Pengirim Surat / Instansi</th>
        <th>Keterangan Disposisi</th>
        <th>Dari Unit</th>
        <th>Untuk Unit</th>
        <th>Tanggal Batas Respon</th>
        <th>Tanggal Respon Unit</th>
        <th>Jumlah Hari Keterlambatan</th>
      </tr>
    </thead>
    <tbody>
        <?php 
          $jumlah_surat = 0;
          $jumlah_keterlambatan_respon = 0;
          $jumlah_ketepatan_respon = 0;
        ?>
      <?php if ($surats): ?>
        <?php foreach ($surats as $srt): ?>
          <tr>
            <td><?=$srt->dspKode?></td>
            <td><?=$srt->smNomorSurat?></td>
            <td><?=basic_date($srt->smTanggalSurat)?></td>
            <td><?=basic_date($srt->dspTanggalDisposisi)?></td>
            <td><?=$srt->smPerihal?></td>
            <td><?=$srt->smTujuan?></td>
            <td><?=$srt->instansiNama?></td>
            <td>
              <?php 
								$dsp_keterangan = $cntrl->disposisiGetDataKeterangan($srt->dspKode); 
								if($dsp_keterangan){
									foreach ($dsp_keterangan as $dspket) {
										echo $dspket->ketDeskripsi.'; ';
									} 
								}
              ?>  
            </td>
            <td> <?php 
                $unit_from = get_unit_master($srt->fromUnitID); 
                echo $unit_from->unitNama;
              ?>   </td>
            <td> <?php 
                $unit_to = get_unit_master($srt->unitID); 
                echo $unit_to->unitNama;
              ?> 
            </td>
            <td><?=basic_date($srt->dspTanggalMinimalRespon)?></td>
            <td><?=basic_date($srt->tanggalKonfirmasi)?></td>
            <td><?=date_difference($srt->dspTanggalMinimalRespon, $srt->tanggalKonfirmasi)?></td>
          </tr>
          <?php
            $total_diffrence = date_difference($srt->dspTanggalMinimalRespon, $srt->tanggalKonfirmasi); 
            $jumlah_surat = $jumlah_surat + 1;
            $jumlah_keterlambatan_respon = $jumlah_keterlambatan_respon + $total_diffrence;
            $jumlah_ketepatan_respon = $jumlah_surat - $jumlah_keterlambatan_respon;
          ?>
        <?php endforeach ?>
      <?php endif ?>
    </tbody>
  </table>
  <script type="text/javascript">
    $('#tableData').dataTable();
  </script>
</div>
  <hr>
  <p>Rekap</p>
  <table class="table table-hovered">
    <thead>
      <tr>
        <th>Jumlah Surat</th>
        <th>Jumlah Keterlambatan</th>
        <th>Jumlah Ketepatan</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><?=$jumlah_surat?></td>
        <td><?=$jumlah_keterlambatan_respon?></td>
        <td><?=$jumlah_ketepatan_respon?></td>
      </tr>
    </tbody>
  </table>
