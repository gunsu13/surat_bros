<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> <?=$page_title?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Menu</a></li>
              <li class="breadcrumb-item"><a href="#">Laporan</a></li>
              <li class="breadcrumb-item active"><?=$page_val?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <form id="formFilter">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Status Supervisi</label>
                        <select class="form-control" id="statusDsp" name="statusDsp">
                          <option value="">- Pilih Status -</option>
                          <option value="1">Disetujui</option>
                          <option value="0">Belum Disetujui</option>
                          <option value="2">Tidak Disetujui</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Instansi ( Asal Surat )</label>
                        <select class="form-control" id="instansiKode" name="instansiKode">
                          <option value="">- Pilih Instansi -</option>
                          <?php foreach ($instansis as $ins): ?>
                            <option value="<?=$ins->instansiKode?>"><?=$ins->instansiNama?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>Periode Tanggal Surat</label>
                        <input type="text" name="dateFiltered" id="dateFiltered" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>-</label><br>
                        <button type="button" id="btnFilter" class="btn btn-primary">Filter</button>
                        <button type="submit" id="btnExport" class="btn btn-success">Excel</button>
                      </div>
                    </div>
                  </div>
                </form>
                <div class="row">
                  <div class="col-md-12">
                  	<div id="data_view">
                  		
                  	</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('modules/layouts/v_layout_footer'); ?>

<script type="text/javascript">
  var statusDsp = $('#statusDsp').val();
  var instansiKode = $('#instansiKode').val();
  var dateFiltered = $('#dateFiltered').val();

	getDataSuratMasuk(statusDsp, instansiKode, dateFiltered);

  $('#btnFilter').click(function(){
    statusDsp = $('#statusDsp').val();
    instansiKode = $('#instansiKode').val();
    dateFiltered = $('#dateFiltered').val();
    getDataSuratMasuk(statusDsp, instansiKode, dateFiltered);
  })

	function getDataSuratMasuk(statusDsp, instansiKode, dateFiltered)
	{
		$.ajax({
        url: '<?=base_url()?>report/surat_keluar_getDataTb',
        type: 'POST',
        dataType: 'text',
        data: 'skStatus='+statusDsp+'&instansiKode='+instansiKode+'&dateFiltered='+dateFiltered
    }) 
    .done(function(data) {
      $('#data_view').html(data);
    })
    .fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
	}


</script>