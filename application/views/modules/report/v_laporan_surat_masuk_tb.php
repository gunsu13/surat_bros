<table class="table table-striped table-hovered" id="tableData" style="font-size: 13px;">
	<thead>
		<tr>
			<th>Kode Surat</th>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Tanggal Terima</th>
			<th>Tujuan Surat</th>
			<th>Asal Instansi</th>
			<th>Perihal</th>
			<th>Keterangan / File</th>
			<th>Status Surat</th>
			<th>Status Disposisi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($surats): ?>
			<?php foreach ($surats as $srt): ?>
				<tr>
					<td><?=$srt->smKode?></td>
					<td><?=$srt->smNomorSurat?></td>
					<td><?=basic_date($srt->smTanggalSurat)?></td>
					<td><?=basic_date($srt->smTanggalTerima)?></td>
					<td><?=$srt->smTujuan?></td>
					<td><?=$srt->instansiNama?></td>
					<td><?=$srt->smPerihal?></td>
					<td><?=$srt->smKeterangan?></td>
					<td><?=$srt->smStatusDetil?></td>
					<td><?=$srt->smStatusDisposisiDetil?></td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>