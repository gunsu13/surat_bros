<table class="table table-striped table-hovered" id="tableData" style="font-size: 13px">
	<thead>
		<tr>
			<th>Kode Disposisi</th>
			<th>Nomor Surat</th>
			<th>Tanggal Surat</th>
			<th>Tanggal Disposisi</th>
			<th>Perihal</th>
			<th>Tujuan Surat</th>
			<th>Pengirim Surat / Instansi</th>
			<th>Keterangan Disposisi</th>
			<th>Unit Terkait</th>
			<th>Status Disposisi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($surats): ?>
			<?php foreach ($surats as $srt): ?>
				<tr>
					<td><?=$srt->dspKode?></td>
					<td><?=$srt->smNomorSurat?></td>
					<td><?=basic_date($srt->smTanggalSurat)?></td>
					<td><?=basic_date($srt->dspTanggalDisposisi)?></td>
					<td><?=$srt->smPerihal?></td>
					<td><?=$srt->smTujuan?></td>
					<td><?=$srt->instansiNama?></td>
					<td>
						<?php 
							$dsp_keterangan = $cntrl->disposisiGetDataKeterangan($srt->dspKode); 
							foreach ($dsp_keterangan as $dspket) {
								echo $dspket->ketDeskripsi.'; ';
							} 
						?>	
					</td>
					<td>
						<?php 
							$dsp_unit = $cntrl->disposisiGetDataUnit($srt->dspKode); 
							foreach ($dsp_unit as $unit) {
								echo $unit->unitNama.'; ';
							} 
						?>
					</td>
					<td><?=$srt->dspStatusDetil?></td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>