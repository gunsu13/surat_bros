<table class="table table-striped table-hovered" id="tableData">
	<thead>
		<tr>
			<th>Username</th>
			<th>Display Name</th>
			<th>Level</th>
			<th>Status</th>
			<th>Unit</th>
			<th>Divisi</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($users): ?>
			<?php foreach ($users as $usr): ?>
				<tr>
					<td><?=$usr->username?></td>
					<td><?=$usr->display_name?></td>
					<td><?=$usr->level_detil?></td>
					<td><?=$usr->status_detil?></td>
					<td><?=$usr->unitNama?></td>
					<td><?=$usr->divisiNama?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectData" id="<?=$usr->username?>">
							<i class="fas fa-check"></i>
						</button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>