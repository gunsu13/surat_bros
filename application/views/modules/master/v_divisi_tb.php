<table class="table table-striped table-hovered" id="tableData">
	<thead>
		<tr>
			<th>Divisi ID</th>
			<th>Nama Divisi</th>
			<th>Keterangan</th>
			<th>Kepala Divisi</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($divisis): ?>
			<?php foreach ($divisis as $dv): ?>
				<tr>
					<td><?=$dv->divisiID?></td>
					<td><?=$dv->divisiNama?></td>
					<td><?=$dv->divisiKeterangan?></td>
					<td><?=$dv->divisiKepala?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectData" id="<?=$dv->divisiID?>">
							<i class="fas fa-check"></i>
						</button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>