<table class="table table-striped table-hovered" id="tableData">
	<thead>
		<tr>
			<th>Keterangan Deskripsi</th>
			<th>Detil</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($kets): ?>
			<?php foreach ($kets as $ket): ?>
				<tr>
					<td><?=$ket->ketDeskripsi?></td>
					<td><?=$ket->ketDeskripsiDetil?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectData" id="<?=$ket->ketID?>">
							<i class="fas fa-check"></i>
						</button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>