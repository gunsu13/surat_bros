<table class="table table-striped table-hovered" id="tableData">
	<thead>
		<tr>
			<th>Kode Instansi</th>
			<th>Nama Instansi</th>
			<th>Alamat</th>
			<th>Tlp</th>
			<th>Email</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($instansis): ?>
			<?php foreach ($instansis as $inst): ?>
				<tr>
					<td><?=$inst->instansiKode?></td>
					<td><?=$inst->instansiNama?></td>
					<td><?=$inst->instansiAlamat?></td>
					<td><?=$inst->instansiNomorTlp?></td>
					<td><?=$inst->instansiEmail?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectData" id="<?=$inst->instansiKode?>">
							<i class="fas fa-check"></i>
						</button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>