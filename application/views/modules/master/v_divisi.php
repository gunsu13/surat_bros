<?php $this->load->view('modules/layouts/v_layout_header'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form role="form" id="formInputDivisi">
                      <div class="form-group">
                        <label for="divisiNama">Nama Divisi</label>
                        <input type="hidden" class="form-control" name="divisiID" id="divisiID">
                        <input type="text" class="form-control" name="divisiNama" id="divisiNama">
                      </div>
                      <div class="form-group">
                        <label for="divisiKepala">Kepala Divisi</label>
                        <input type="text" class="form-control" name="divisiKepala" id="divisiKepala">
                      </div>
                      <div class="form-group">
                        <label for="divisiKeterangan">Notes</label>
                        <textarea class="form-control" rows="3" name="divisiKeterangan" id="divisiKeterangan"></textarea>
                      </div>
                    </form>
                  </div>
                </div>
                <hr>
                <button type="button" class="btn btn-default" id="btnMdDivisi">
                  List Divisi
                </button>
                <div class="btn btn-group float-right">
                	
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
	                <button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
                </div>
  
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal -->
    <div class="modal fade" id="mdDivisi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Divisi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	var edit = false;

	$('#btnHapus').click(function(){
		var divisiID = $('#divisiID').val();

		if (divisiID == '') 
		{
			$("#formInputDivisi")[0].reset();
		}else{
			Swal.fire({
			  title: 'Apakah anda yakin?',
			  text: "Data yang dihapus akan hilang dari list",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Hapus !'
			}).then((result) => {
			  if (result.value) {

			  	$.ajax({
			        url: '<?=base_url()?>master/divisiDelete/'+divisiID,
			        type: 'GET',
			        dataType: 'html',
	        		async: false
			    }) 
			    .done(function(data) {
			    	console.log(data);
			     	var obj = JSON.parse(data);

			     	if (obj.status == 'true') 
			     	{
			     		edit = false;
			     		$("#formInputDivisi")[0].reset();

			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}else{
			     		Swal.fire(
						  'Info',
						  obj.message,
					  	  'warning'
						);
			     	}
			    })
			  	.fail(function (jqXHR, textStatus, error) {
			      	console.log("Post error: " + error);
			  	});

			    
			  }
			})
		}
	})

	$('body').on('click', '.btnSelectData', function(){
		var divisiID = $(this).attr('id');
		var dataDivisi = getDataDivisiSinggle(divisiID);

		var obj = JSON.parse(dataDivisi);

		$('#divisiID').val(obj.divisiID);
		$('#divisiNama').val(obj.divisiNama);
		$('#divisiKeterangan').val(obj.divisiKeterangan);
		$('#divisiKepala').val(obj.divisiKepala);

		edit = true;

		$('#mdDivisi').modal('hide');
	})

	$('#btnMdDivisi').click(function(){
		getDataDivisi();
		$('#mdDivisi').modal('show');
	})

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInputDivisi').serialize();

		if (edit == false) 
		{
			$.ajax({
		        url: '<?=base_url()?>master/divisiCreate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInputDivisi")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  	  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}else{
			$.ajax({
		        url: '<?=base_url()?>master/divisiUpdate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInputDivisi")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
					
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  	  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}
	})

	function getDataDivisi()
	{
		$.get('<?=base_url()?>master/divisiGetDataTabel', function(data){
			$('#data_view').html(data);
		})
	}

	function getDataDivisiSinggle(divisiID)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>master/divisiGetDataSinggle/'+divisiID,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}
</script>