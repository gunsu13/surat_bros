<?php $this->load->view('modules/layouts/v_layout_header'); ?>
  <!-- Content Wrapper. Contains page content -->
   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form role="form" id="formInputUnit">
                      <div class="form-group">
                        <label for="unitNama">Nama Unit</label>
                        <input type="text" class="form-control" name="unitNama" id="unitNama">
                        <input type="hidden" class="form-control" name="unitID" id="unitID">
                      </div>
                      <div class="form-group">
                        <label for="unitKepala">Divisi</label>
                        <select class="form-control" name="divisiID" id="divisiID">
                        	<option value="">- Pilih Divisi -</option>
                        	<?php foreach ($divisis as $dv): ?>
                        		<option value="<?=$dv->divisiID?>"><?=$dv->divisiNama?></option>
                        	<?php endforeach ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="unitKepala">Kepala Unit</label>
                        <input type="text" class="form-control" name="unitKepala" id="unitKepala">
                      </div>
                      <div class="form-group">
                        <label for="unitKeterangan">Notes</label>
                        <textarea class="form-control" rows="3" name="unitKeterangan" id="unitKeterangan"></textarea>
                      </div>
                    </form>
                  </div>
                </div>
                <hr>
                <button type="button" class="btn btn-default" id="btnMdUnit">
                  List Unit
                </button>
                <div class="btn btn-group float-right">
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
	                <button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal -->
    <div class="modal fade" id="mdUnit">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Unit</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	var edit = false;

	$('#btnHapus').click(function(){
		var unitID = $('#unitID').val();

		if (unitID == '') 
		{
			$("#formInputUnit")[0].reset();
		}else{
			Swal.fire({
			  title: 'Apakah anda yakin?',
			  text: "Data yang dihapus akan hilang dari list",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Hapus !'
			}).then((result) => {
			  if (result.value) {

			  	$.ajax({
			        url: '<?=base_url()?>master/unitDelete/'+unitID,
			        type: 'GET',
			        dataType: 'html',
	        		async: false
			    }) 
			    .done(function(data) {
			    	console.log(data);
			     	var obj = JSON.parse(data);

			     	if (obj.status == 'true') 
			     	{
			     		edit = false;
			     		$("#formInputUnit")[0].reset();

			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}else{
			     		Swal.fire(
						  'Info',
						  obj.message,
					  		'warning'
						);
			     	}
			    })
			  	.fail(function (jqXHR, textStatus, error) {
			      	console.log("Post error: " + error);
			  	});

			    
			  }
			})
		}
	})

	$('body').on('click', '.btnSelectData', function(){
		var unitID = $(this).attr('id');
		var data = getDataSinggle(unitID);

		var obj = JSON.parse(data);

		$('#unitID').val(obj.unitID);
		$('#unitNama').val(obj.unitNama);
		$('#divisiID').val(obj.divisiID);
		$('#unitKepala').val(obj.unitKepala);
		$('#unitKeterangan').val(obj.unitKeterangan);

		edit = true;

		$('#mdUnit').modal('hide');
	})

	$('#btnMdUnit').click(function(){
		getDataTabel();
		$('#mdUnit').modal('show');
	})

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInputUnit').serialize();

		if (edit == false) 
		{
			$.ajax({
		        url: '<?=base_url()?>master/unitCreate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInputUnit")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}else{
			$.ajax({
		        url: '<?=base_url()?>master/unitUpdate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInputUnit")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
					
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}
	})

	function getDataTabel()
	{
		$.get('<?=base_url()?>master/unitGetDataTabel', function(data){
			$('#data_view').html(data);
		})
	}

	function getDataSinggle(unitID)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>master/unitGetDataSinggle/'+unitID,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}
</script>