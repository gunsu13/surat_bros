<?php $this->load->view('modules/layouts/v_layout_header'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form role="form" id="formInput">
                      <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" id="username">
                      </div>
                      <div class="form-group">
                        <label for="display_name">Display Name</label>
                        <input type="text" class="form-control" name="display_name" id="display_name">
                      </div>
                      <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" id="password">
                      </div>
                      <div class="form-group">
                        <label for="password">Re Password</label>
                        <input type="password" class="form-control" name="repassword" id="repassword">
                      </div>
                      <div class="form-group">
                        <label>Unit</label>
                        <select class="form-control" id="unitID" name="unitID">
                          <option value="">- Pilih Unit -</option>
                          <?php foreach ($units as $unit): ?>
                            <option value="<?=$unit->unitID?>"><?=$unit->unitNama?></option>
                          <?php endforeach ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="password">Level</label>
                        <select class="form-control" name="level" id="level">
                        	<option value="">- Pilih Level -</option>
                        	<option value="1">Admin</option>
                        	<option value="2">Sekretaris</option>
                        	<option value="3">Direktur</option>
                        	<option value="4">Unit</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="password">Status</label>
                        <select class="form-control" name="status" id="status">
                        	<option value="">- Pilih Level -</option>
                        	<option value="1">Aktif</option>
                        	<option value="0">Non Aktif</option>
                        </select>
                      </div>
                    </form>
                  </div>
                </div>
                <hr>
                <button type="button" class="btn btn-default" id="btnMdDivisi">
                  List User
                </button>
                <div class="btn btn-group float-right">
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
	                <button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
                </div>
  
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- modal -->
    <div class="modal fade" id="mdDivisi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Divisi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	var edit = false;

	$('#btnHapus').click(function(){
		var username = $('#username').val();

		if (username == '') 
		{
			$("#formInput")[0].reset();
		}else{
			Swal.fire({
			  title: 'Apakah anda yakin?',
			  text: "Data yang dihapus akan hilang dari list",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Hapus !'
			}).then((result) => {
			  if (result.value) {

			  	$.ajax({
			        url: '<?=base_url()?>master/userDeleted/'+username,
			        type: 'GET',
			        dataType: 'html',
	        		async: false
			    }) 
			    .done(function(data) {
			    	console.log(data);
			     	var obj = JSON.parse(data);

			     	if (obj.status == 'true') 
			     	{
			     		edit = false;
			     		$("#formInput")[0].reset();
						$('#username').prop('readonly', false);

			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}else{
			     		Swal.fire(
						  'Info',
						  obj.message,
					  	  'warning'
						);
			     	}
			    })
			  	.fail(function (jqXHR, textStatus, error) {
			      	console.log("Post error: " + error);
			  	});			  }
			})
		}
	})

	$('body').on('click', '.btnSelectData', function(){
		var username = $(this).attr('id');
		var dataUser = getDataSinggle(username);

		var obj = JSON.parse(dataUser);

		$('#username').val(obj.username);
		$('#username').prop('readonly', true);
		$('#display_name').val(obj.display_name);
		$('#status').val(obj.status);
		$('#level').val(obj.level);
		$('#unitID').val(obj.unitID);

		edit = true;

		$('#mdDivisi').modal('hide');
	})

	$('#btnMdDivisi').click(function(){
		getData();
		$('#mdDivisi').modal('show');
	})

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInput').serialize();

		if (edit == false) 
		{
			$.ajax({
		        url: '<?=base_url()?>master/userCreate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  	  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}else{
			$.ajax({
		        url: '<?=base_url()?>master/userUpdate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
					$('#username').prop('readonly', false);
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
					
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  	  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}
	})

	function getData()
	{
		$.get('<?=base_url()?>master/userGetData', function(data){
			$('#data_view').html(data);
		})
	}

	function getDataSinggle(username)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>master/userGetDataDetil/'+username,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}
</script>
