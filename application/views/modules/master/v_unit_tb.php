<table class="table table-striped table-hovered" id="tableData">
	<thead>
		<tr>
			<th>Unit ID</th>
			<th>Unit Nama</th>
			<th>Divisi</th>
			<th>Keterangan</th>
			<th>Kepala Unit</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		<?php if ($units): ?>
			<?php foreach ($units as $unit): ?>
				<tr>
					<td><?=$unit->unitID?></td>
					<td><?=$unit->unitNama?></td>
					<td><?=$unit->divisiNama?></td>
					<td><?=$unit->unitKeterangan?></td>
					<td><?=$unit->unitKepala?></td>
					<td>
						<button type="button" class="btn btn-primary btn-sm btnSelectData" id="<?=$unit->unitID?>">
							<i class="fas fa-check"></i>
						</button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>
<script type="text/javascript">
	$('#tableData').dataTable();
</script>