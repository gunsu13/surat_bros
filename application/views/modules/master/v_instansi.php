<?php $this->load->view('modules/layouts/v_layout_header'); ?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <form role="form" id="formInput">
                      <div class="form-group">
                        <label for="instansiKode">Kode Instansi</label>
                        <input type="text" class="form-control" name="instansiKode" id="instansiKode">
                      </div>
                      <div class="form-group">
                        <label for="instansiNama">Nama Instansi</label>
                        <input type="text" class="form-control" name="instansiNama" id="instansiNama">
                      </div>
                      <div class="form-group">
                        <label for="instansiAlamat">Alamat Instansi</label>
                        <input type="text" class="form-control" name="instansiAlamat" id="instansiAlamat">
                      </div>
                      <div class="form-group">
                        <label for="instansiNoTlp">No Telp Instansi</label>
                        <input type="text" class="form-control" name="instansiNomorTlp" id="instansiNomorTlp">
                      </div>
                      <div class="form-group">
                        <label for="instansiEmail">Email Instansi</label>
                        <input type="text" class="form-control" name="instansiEmail" id="instansiEmail">
                      </div>
                    </form>
                  </div>
                </div>
                <hr>
                <button type="button" class="btn btn-default" id="btnMdUnit">
                  List Unit
                </button>
                <div class="btn btn-group float-right">
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
	                <button type="button" class="btn btn-danger" id="btnHapus">Hapus</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- modal -->
    <div class="modal fade" id="mdInstansi">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Data Instansi</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
            	<div class="col-md-12">
            		<div id="data_view">
            			
            		</div>
            	</div>
            </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  <!-- /.modal -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	var edit = false;

	$('#btnHapus').click(function(){
		var instansiKode = $('#instansiKode').val();

		if (instansiKode == '') 
		{
			$("#formInput")[0].reset();
		}else{
			Swal.fire({
			  title: 'Apakah anda yakin?',
			  text: "Data yang dihapus akan hilang dari list",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Ya, Hapus !'
			}).then((result) => {
			  if (result.value) {

			  	$.ajax({
			        url: '<?=base_url()?>master/instansiDelete/'+instansiKode,
			        type: 'GET',
			        dataType: 'html',
	        		async: false
			    }) 
			    .done(function(data) {
			    	console.log(data);
			     	var obj = JSON.parse(data);

			     	if (obj.status == 'true') 
			     	{
			     		edit = false;
			     		$("#formInput")[0].reset();

			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}else{
			     		Swal.fire(
						  'Info',
						  obj.message,
						  'success'
						);
			     	}
			    })
			  	.fail(function (jqXHR, textStatus, error) {
			      	console.log("Post error: " + error);
			  	});

			    
			  }
			})
		}
	})

	$('body').on('click', '.btnSelectData', function(){
		var instansiKode = $(this).attr('id');
		var data = getDataSinggle(instansiKode);

		var obj = JSON.parse(data);

		$('#instansiKode').val(obj.instansiKode);
		$('#instansiNama').val(obj.instansiNama);
		$('#instansiAlamat').val(obj.instansiAlamat);
		$('#instansiNomorTlp').val(obj.instansiNomorTlp);
		$('#instansiEmail').val(obj.instansiEmail);

		edit = true;

		$('#mdInstansi').modal('hide');
	})

	$('#btnMdUnit').click(function(){
		getDataTabel();
		$('#mdInstansi').modal('show');
	})

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInput').serialize();

		if (edit == false) 
		{
			$.ajax({
		        url: '<?=base_url()?>master/instansiCreate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}else{
			$.ajax({
		        url: '<?=base_url()?>master/instansiUpdate',
		        type: 'POST',
		        dataType: 'text',
		        data: dataSerialize
		    }) 
		    .done(function(data) {
		    	console.log(data);
		     	var obj = JSON.parse(data);

		     	if (obj.status == 'true') 
		     	{
		     		edit = false;
		     		$("#formInput")[0].reset();
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'success'
					);
					
		     	}else{
		     		Swal.fire(
					  'Info',
					  obj.message,
					  'warning'
					);
		     	}
		    })
		  	.fail(function (jqXHR, textStatus, error) {
		      	console.log("Post error: " + error);
		  	});
		}
	})

	function getDataTabel()
	{
		$.get('<?=base_url()?>master/instansiGetDataTabel', function(data){
			$('#data_view').html(data);
		})
	}

	function getDataSinggle(unitID)
	{
		var result = null;

		$.ajax({
	        url: '<?=base_url()?>master/instansiGetDataSinggle/'+unitID,
	        type: 'GET',
	        dataType: 'html',
	        async: false,
	        success: function(data) {
	            result = data;
	        } 
	     });

		return result;
	}
</script>