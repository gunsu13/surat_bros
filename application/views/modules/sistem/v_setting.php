<?php $this->load->view('modules/layouts/v_layout_header'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">

    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <form role="form" id="formInput">
	                <div class="row">
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Kode Perusahaan</label>
						  	<input type="text" name="company_kode" id="company_kode" class="form-control" value="<?=$this->session->userdata('company_kode')?>" readonly>
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Nama Perusahaan</label>
						  	<input type="text" name="company_name" id="company_name" class="form-control">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Alamat</label>
						  	<input type="text" name="company_address" id="company_address" class="form-control">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Telp</label>
						  	<input type="text" name="company_telp" id="company_telp" class="form-control">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Email</label>
						  	<input type="text" name="comapny_email" id="comapny_email" class="form-control">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Web</label>
						  	<input type="text" name="company_web" id="company_web" class="form-control">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Logo</label>
						  	<input type="text" name="company_logo" id="company_logo" class="form-control">
						  </div>
						</div>
						<div class="col-md-6">
						  <div class="form-group">
						  	<label>Logo Dir</label>
						  	<input type="text" name="company_logo_dir" id="company_logo_dir" class="form-control">
						  </div>
						</div>
	                </div>
            	</form>
                <hr>
                <button type="button" class="btn btn-default" id="btnMdDivisi">
                  List Of Setting
                </button>
                <div class="btn btn-group float-right">
	                <button type="button" class="btn btn-primary" id="btnSimpan">Simpan</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php $this->load->view('modules/layouts/v_layout_footer'); ?>
<script type="text/javascript">
	var company_kode = $('#company_kode').val();

	getDataSetting(company_kode);

	$('#btnSimpan').click(function(){
		var dataSerialize = $('#formInput').serialize();

		saveUpdateSetting(dataSerialize);
	})

	function getDataSetting(company_kode)
	{
		$.get('<?=base_url()?>sistem/settingGetDataDetil/'+company_kode, function(data){
			var obj = JSON.parse(data);
			console.log(obj);

			$('#company_name').val(obj.company_name);
			$('#company_address').val(obj.company_address);
			$('#company_telp').val(obj.company_telp);
			$('#comapny_email').val(obj.comapny_email);
			$('#company_web').val(obj.company_web);
			$('#company_logo').val(obj.company_logo);
			$('#company_logo_dir').val(obj.company_logo_dir);
		})
	}

	function saveUpdateSetting(dataSerialize)
	{
		$.ajax({
	        url: '<?=base_url()?>sistem/settingUpdate',
	        type: 'POST',
	        dataType: 'text',
	        data: dataSerialize
	    }) 
	    .done(function(data) {
	    	console.log(data);
	     	var obj = JSON.parse(data);

	     	if (obj.status == 'true') 
	     	{

				getDataSetting(company_kode);
	     		Swal.fire(
				  'Info',
				  obj.message,
				  'success'
				);
	     	}else{
	     		Swal.fire(
				  'Info',
				  obj.message,
				  	  'warning'
				);
	     	}
	    })
	  	.fail(function (jqXHR, textStatus, error) {
	      	console.log("Post error: " + error);
	  	});
	}
</script>