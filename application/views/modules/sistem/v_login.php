
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi Disposisi dan Arsip Surat Resmi</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box" style="margin-top: -100px;">
  <div class="login-logo">
    <a href="<?=base_url()?>assets/index2.html"><b>Adsip</b>LTE</a> <br>
    <?php $setting = get_setting_master('BROS'); echo $setting->company_name; ?>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form id="formLogin">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" id="username" name="username" autocomplete="off">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="button" class="btn btn-primary btn-block" id="btnLogin">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
  <div class="card">
    <div class="card-body">
      Created and Developed By Information System Development Division Bali Royal Hospital
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?=base_url()?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url()?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url()?>assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dist/js/adminlte.min.js"></script>
<script type="text/javascript">
  $('#btnLogin').click(function(){
    var dataSerialize = $('#formLogin').serialize();
    auth(dataSerialize);
  })

  $('#password').keypress(function(e){
    var dataSerialize = $('#formLogin').serialize();

    if(e.which == 13) {
      auth(dataSerialize);
    }
  });

  function auth(dataSerialize)
  {
    $.ajax({
      url: '<?=base_url()?>Auth/auth_do',
      type: 'POST',
      dataType: 'text',
      data: dataSerialize

    }).done(function(data) {
      var obj = JSON.parse(data);

      console.log(obj);

      if (obj.status == 'true') {
        location.href='<?=base_url().'/Dashboard'?>';
      }else{
        Swal.fire(
          'Info',
          obj.message,
          'success'
        );

        $('#password').val('');
        $('#password').focus();
      }

    }).fail(function (jqXHR, textStatus, error) {
        console.log("Post error: " + error);
    });
  }
</script>
</body>
</html>
