<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getRecordOnId'))
{
    function getRecordOnId($table, $primaryKey, $pharam){

        $ci =& get_instance();
        $ci->db->select('*');
        $ci->db->from($table);
        $ci->db->where($primaryKey, $pharam);

        $query = $ci->db->get();
        return $query->row();
    }
}

if ( ! function_exists('get_unit_master'))
{
    function get_unit_master($unitID){

        $ci =& get_instance();
        $ci->load->model('M_master');

        $result = $ci->M_master->getDataProperty('t_unit', 'unitID', $unitID, false, 'unit');

        return $result;
    }
}

if ( ! function_exists('get_setting_master'))
{
    function get_setting_master($company_kode = ''){

        $ci =& get_instance();
        $ci->load->model('M_master');

        $result = $ci->M_master->getDataProperty('t_settings', 'company_kode', $company_kode, false, 'company_');

        return $result;
    }
}

if ( ! function_exists('getCountFile'))
{
    function getCountFile($smKode = ''){
        $return = 0;

        if ($smKode == '') {
            return $return;
        }else{

            $ci =& get_instance();
            $ci->load->model('M_master');
            $data = $ci->M_master->getDataFile(array('refKode' => $smKode));

            if ($data) {
                foreach ($data as $dt) {
                    $return = $return + 1;
                }
            }

            return $return;
        }
    }
}