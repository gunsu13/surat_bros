<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('basic_date'))
{
    function basic_date($date = '')
    {
        // jika date == '' maka return current date
        $return = ($date != '' ) ? date('d-m-Y', strtotime($date)) : date('d-m-Y');
        return $return;
    }
}

if ( ! function_exists('database_date'))
{
    function database_date($date)
    {
        $return = ($date != '' ) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        return $return;
    }
}

if ( ! function_exists('database_datetime'))
{
    function database_datetime($date)
    {
        $return = ($date != '' ) ? date('Y-m-d H:i:s', strtotime($date)) : date('Y-m-d H:i:s');
        return $return;
    }
}

if ( ! function_exists('date_difference'))
{
    function date_difference($date, $date_to_diff)
    {
        $return = 0;
        $function1 = new dateTime($date);
        $function2 = new dateTime($date_to_diff);
        $diffrenece = $function1->diff($function2);
        if ($diffrenece->invert == 0) {
            $return = $diffrenece->d;
        }else{
            $return = 0;
        }
        return $return;
    }
}
