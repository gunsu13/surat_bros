<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_master extends CI_Model {

	public function insertMaster($tabel, $data)
	{
		if ($this->db->insert($tabel, $data)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function updateMaster($tabel, $data, $parameter_filed, $parameter_values)
	{
		if ($this->db->where($parameter_filed, $parameter_values)->update($tabel, $data))
		{
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function deleteMaster($tabel, $parameter_filed, $parameter_values)
	{
		if ($this->db->where($parameter_filed, $parameter_values)->delete($tabel)) {
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function getDataProperty($tabel, $primary_field = '', $primary_value = '', $deletedDate = false, $field_preffix)
	{
		$this->db->select('*');
		$this->db->from($tabel);

		if ($deletedDate == true) {	
			$this->db->where($field_preffix.'DeletedDate IS NULL');
		}

		if ($primary_field != '') {
			$this->db->where($primary_field, $primary_value);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}

		return $result;
	}

	public function getDataDivisi($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('t_divisi');

		if (array_key_exists('deletedDate', $pharam)) 
		{
			$this->db->where('divisiDeletedDate IS NOT NULL');
		}

		if (array_key_exists('divisiID', $pharam)) 
		{
			$this->db->where('divisiID', $pharam['divisiID']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}

		return $result;
	}

	public function getDataVSuratMasuk($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('vsuratmasukmaster');

		if (array_key_exists('smStatusDisposisi', $pharam)) 
		{
			if ($pharam['smStatusDisposisi'] != '') {
				$this->db->where('smStatusDisposisi', $pharam['smStatusDisposisi']);
			}
		}

		if (array_key_exists('smStatusArsip', $pharam)) 
		{
			if ($pharam['smStatusArsip'] != '') {
				$this->db->where('smStatusArsip', $pharam['smStatusArsip']);
			}
		}

		if (array_key_exists('instansiKode', $pharam)) 
		{
			if ($pharam['instansiKode'] != '') {
				$this->db->where('instansiKode', $pharam['instansiKode']);
			}
		}

		if (array_key_exists('smStatus', $pharam)) 
		{
			if ($pharam['smStatus'] != '') {
				$this->db->where('smStatus', $pharam['smStatus']);
			}
		}

		if (array_key_exists('date_filter', $pharam)) {
			if ($pharam['date_filter'] != '') {
				$this->db->where('smTanggalTerima >= ', $pharam['date_start']);
				$this->db->where('smTanggalTerima <= ', $pharam['date_end']);
			}
		}

		if (array_key_exists('tahun_surat', $pharam)) {
			if ($pharam['tahun_surat'] != '') {
				$this->db->where('YEAR(smTanggalSurat) = ', $pharam['tahun_surat']);
			}
		}

		if (array_key_exists('deletedDate', $pharam)) 
		{
			if ($pharam['deletedDate'] != '') {
				$this->db->where('smDeletedDate IS NULL');
			}
		}

		if (array_key_exists('smKode', $pharam)) 
		{
			$this->db->where('smKode', $pharam['smKode']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{

			if (array_key_exists('orderBy_date', $pharam)) 
			{
				if ($pharam['orderBy_date'] != '') {
					$this->db->order_by('smTanggalTerima', $pharam['orderBy_date']);
				}
			}

			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}

		return $result;
	}

	public function getDataVSuratKeluar($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('vsuratkeluarmaster');

		if (array_key_exists('skStatus', $pharam)) 
		{
			if ($pharam['skStatus'] != '') {
				$this->db->where('skStatus', $pharam['skStatus']);
			}
		}
		
		if (array_key_exists('instansiKode', $pharam)) 
		{
			if ($pharam['instansiKode'] != '') {
				$this->db->where('instansiKode', $pharam['instansiKode']);
			}
		}

		if (array_key_exists('date_filter', $pharam)) {
			if ($pharam['date_filter'] != '') {
				$this->db->where('skTanggalKeluar >= ', $pharam['date_start']);
				$this->db->where('skTanggalKeluar <= ', $pharam['date_end']);
			}
		}

		if (array_key_exists('skjID', $pharam)) {
			if ($pharam['skjID'] != '') {
				$this->db->where('skjID', $pharam['skjID']);
			}
		}

		if (array_key_exists('tahun_surat', $pharam)) {
			if ($pharam['tahun_surat'] != '') {
				$this->db->where('YEAR(skTanggalKeluar)', $pharam['tahun_surat']);
			}
		}

		if (array_key_exists('order_by', $pharam)) {
			if ($pharam['order_by'] != '') {
				$this->db->order_by($pharam['order_by'], $pharam['order_by_prop']);
			}
		}

		if (array_key_exists('limit', $pharam)) {
			if ($pharam['limit'] != '') {
				$this->db->limit($pharam['limit'], $pharam['limit_offset']);
			}
		}

		if (array_key_exists('skKode', $pharam)) 
		{
			$this->db->where('skKode', $pharam['skKode']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}

		return $result;
	}

	public function getDataFile($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('t_file');

		if (array_key_exists('deletedDate', $pharam)) 
		{
			$this->db->where('fileDeletedDate IS NOT NULL');
		}

		if (array_key_exists('refKode', $pharam)) 
		{
			$this->db->where('refKode', $pharam['refKode']);
		}

		if (array_key_exists('fileKode', $pharam)) 
		{
			$this->db->where('fileKode', $pharam['fileKode']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}

		return $result;
	}

	public function getDataVDisposisi($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('vdisposisimaster');

		if (array_key_exists('dspStatus', $pharam)) 
		{
			if ($pharam['dspStatus'] != '') {
				$this->db->where('dspStatus', $pharam['dspStatus']);
			}
		}

		if (array_key_exists('smStatusArsip', $pharam)) 
		{
			if ($pharam['smStatusArsip'] != '') {
				$this->db->where('smStatusArsip', $pharam['smStatusArsip']);
			}
		}

		if (array_key_exists('date_filter', $pharam)) {
			if ($pharam['date_filter'] != '') {
				$this->db->where('dspTanggalDisposisi >= ', $pharam['date_start']);
				$this->db->where('dspTanggalDisposisi <= ', $pharam['date_end']);
			}
		}

		if (array_key_exists('dspKode', $pharam)) 
		{
			$this->db->where('dspKode', $pharam['dspKode']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}

		return $result;
	}

	public function getDataVDisposisiMappingUnit($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('vdisposisimappingunit');

		if (array_key_exists('dspKode', $pharam)) 
		{
			if ($pharam['dspKode'] != '') {
				$this->db->where('dspKode', $pharam['dspKode']);
			}
		}

		if (array_key_exists('smKode', $pharam)) 
		{
			if ($pharam['smKode'] != '') {
				$this->db->where('smKode', $pharam['smKode']);
			}
		}

		if (array_key_exists('unitID', $pharam)) 
		{
			if ($pharam['unitID'] != '') {
				$this->db->where('unitID', $pharam['unitID']);
			}
		}

		if (array_key_exists('divisiID', $pharam)) 
		{
			if ($pharam['divisiID'] != '') {
				$this->db->where('divisiID', $pharam['divisiID']);
			}
		}

		if (array_key_exists('isNotNull', $pharam)) 
		{
			if ($pharam['isNotNull'] == 'tanggalKonfirmasi') {
				$this->db->where('tanggalKonfirmasi IS NOT NULL');
			}
		}

		if (array_key_exists('order_by', $pharam)) 
		{
			if ($pharam['order_by'] != '') {
				$this->db->order_by( $pharam['order_by'], $pharam['order_type']);
			}
		}


		if (array_key_exists('dspUnitID', $pharam)) 
		{
			$this->db->where('dspUnitID', $pharam['dspUnitID']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}
		return $result;
	}

	public function getDataVDisposisiMappingKeterangan($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('vdisposisimappingketerangan');

		if (array_key_exists('dspKode', $pharam)) 
		{
			if ($pharam['dspKode'] != '') {
				$this->db->where('dspKode', $pharam['dspKode']);
			}
		}

		$query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->result():FALSE;
		
		return $result;
	}

	public function getDataSetting($pharam = array())
	{
		$this->db->select('*');
		$this->db->from('t_settings');

		if (array_key_exists('company_kode', $pharam)) 
		{
			$this->db->where('company_kode', $pharam['company_kode']);
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->row():FALSE;
		}else{
			$query = $this->db->get();
            $result = ($query->num_rows() > 0)?$query->result():FALSE;
		}
		return $result;
	}

	/* this modal is going to create all code */
	public function genMaster($tabel, $key_field, $date_field)
	{
		$q  = $this->db->query("SELECT MAX(RIGHT(".$key_field.", 4)) AS idmax FROM ".$tabel." WHERE DATE(".$date_field.") = '".date('Y-m-d')."'");
        $kd = ""; //kode awal
        if($q->num_rows() > 0){ //jika data ada
            foreach($q->result() as $k){
                $tmp = ((int)$k->idmax)+1; //string kode diset ke integer dan ditambahkan 1 dari kode terakhir
                $kd = sprintf("%04s", $tmp); //kode ambil 4 karakter terakhir
            }
        }else{ //jika data kosong diset ke kode awal
            $kd = "0001";
        }
        //gabungkan string dengan kode yang telah dibuat tadi
        return $kd;
	}
}
