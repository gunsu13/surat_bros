<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }

    //Page header
    public function Header() {
        $ci =& get_instance();
        $setting = get_setting_master($ci->session->userdata('company_kode'));
        // Logo
        $image_file = $_SERVER['DOCUMENT_ROOT'].$setting->company_logo_dir;
        $this->Image($image_file, 10, 2, 30, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 0, '', 0, 1, 'C', 0, '', 0);
        $this->Cell(0, 0, $setting->company_name, 0, 1, 'C', 0, '', 1);
        $this->SetFont('helvetica', '', 11);
        $this->Cell(0, 0,  $setting->company_address, 0, 1, 'C', 0, '', 3);

        // $image_file2 = $_SERVER['DOCUMENT_ROOT'].'/suratbros/assets/images/bros2.jpg';
        // $this->Image($image_file2, 165, 2, 30, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

    // Page footer
    public function Footer() {
        $ci =& get_instance();
        $setting = get_setting_master($ci->session->userdata('company_kode'));

        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages().' | email : '.$setting->comapny_email.' | web : '.$setting->company_web.' ', 0, false, 'L', 0, '', 0, false, 'T', 'M');
    }
}