<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surat extends CI_Controller {
	
	public function __construct() {
	    parent::__construct();
	    $this->load->model('M_master');	
	}

	public function suratMasuk()
	{
		$data['page_title'] = 'Insert Surat Masuk';
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'surat_masuk';

		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');
		$this->load->view('modules/surat/v_surat_masuk', $data);
	}

	public function suratMasukGetDataTabel($tahun)
	{	
		$data['cntrl'] = $this;
		$data['surats'] = $this->M_master->getDataVSuratMasuk(array('smStatusArsip' => '0', 'deletedDate' => 'true', 'orderBy_date' => 'desc', 'tahun_surat' => $tahun));
		
		$this->load->view('modules/surat/v_surat_masuk_tb', $data);
	}

	public function suratMasukGetDataSinggle($smKode)
	{
		$data = $this->M_master->getDataProperty('t_surat_masuk', 'smKode', $smKode, false, '');

		$data->dateConverted_tanggalTerima = date('m/d/Y', strtotime($data->smTanggalTerima));
		$data->dateConverted_tanggalSurat = date('m/d/Y', strtotime($data->smTanggalSurat));

		echo json_encode($data);
	}

	public function suratMasukGetDataCountFile($smKode)
	{
		$return = 0;
		$data = $this->M_master->getDataFile(array('refKode' => $smKode));

		if ($data) {
			foreach ($data as $dt) {
				$return = $return + 1;
			}
		}

		return $return;
	}

	public function suratMasukCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('smNomorSurat', 'smNomorSurat', 'required');
			$this->form_validation->set_rules('smPerihal', 'smPerihal', 'required');
			$this->form_validation->set_rules('smTanggalSurat', 'smTanggalSurat', 'required');
			$this->form_validation->set_rules('smTanggalTerima', 'smTanggalTerima', 'required');
			$this->form_validation->set_rules('smTujuan', 'smTujuan', 'required');
			$this->form_validation->set_rules('instansiKode', 'kodePengirim', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$generatedSmKode = 'SM-'.date('ymd').'-'.$this->M_master->genMaster('t_surat_masuk', 'smKode', 'smCreatedDate');

				$data_row = array(
					'smKode' => $generatedSmKode,
					'smNomorSurat' => $_POST['smNomorSurat'],
					'smPerihal' => $_POST['smPerihal'],
					'smTanggalSurat' => date('Y-m-d', strtotime($_POST['smTanggalSurat'])),
					'smTanggalTerima' => date('Y-m-d', strtotime($_POST['smTanggalTerima'])),
					'smTujuan' => $_POST['smTujuan'],
					'kodePengirim' => $_POST['instansiKode'],
					'smKeterangan' => $_POST['smKeterangan'],
					'smStatus' => 0,
					'smStatusDisposisi' => 0,
					'smCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_surat_masuk', $data_row);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data surat berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function suratMasukUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {

			$this->form_validation->set_rules('smNomorSurat', 'smNomorSurat', 'required');
			$this->form_validation->set_rules('smPerihal', 'smPerihal', 'required');
			$this->form_validation->set_rules('smTanggalSurat', 'smTanggalSurat', 'required');
			$this->form_validation->set_rules('smTanggalTerima', 'smTanggalTerima', 'required');
			$this->form_validation->set_rules('smTujuan', 'smTujuan', 'required');
			$this->form_validation->set_rules('instansiKode', 'kodePengirim', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'smNomorSurat' => $_POST['smNomorSurat'],
					'smPerihal' => $_POST['smPerihal'],
					'smTanggalSurat' => date('Y-m-d', strtotime($_POST['smTanggalSurat'])),
					'smTanggalTerima' => date('Y-m-d', strtotime($_POST['smTanggalTerima'])),
					'smTujuan' => $_POST['smTujuan'],
					'kodePengirim' => $_POST['instansiKode'],
					'smKeterangan' => $_POST['smKeterangan'],
					'smUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_surat_masuk', $data_row, 'smKode', $_POST['smKode']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data surat berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function suratMasukDelete($smKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($smKode != '') {
			$data_row = array(
				'smDeletedDate' => date('Y-m-d H:i:s')
			);
			$exce = $this->M_master->updateMaster('t_surat_masuk', $data_row, 'smKode', $smKode);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data surat berhasil di input');
			}
		}
		
		echo json_encode($response);
	}


	public function suratKeluar()
	{
		$data['page_title'] = 'Insert Surat Keluar';
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'surat_keluar';

		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');

		$data['jenis_surats'] = $this->M_master->getDataProperty('t_surat_keluar_jenis','','', true, 'skj');

		$this->load->view('modules/surat/v_surat_keluar', $data);
	}

	public function suratKeluarGetDataTabel(string $jenis_surat, String $tahun_surat)
	{
		if($jenis_surat == '-')
		{
			$data['cntrl'] = $this;
			$data['surats'] = $this->M_master->getDataVSuratKeluar(['tahun_surat' => $tahun_surat]);	
		}else{
			$data['cntrl'] = $this;
			$data['surats'] = $this->M_master->getDataVSuratKeluar(['skjID' => $jenis_surat, 'tahun_surat' => $tahun_surat]);	
		}
		
		$this->load->view('modules/surat/v_surat_keluar_tb', $data);
	}

	public function suratKeluarGetDataSinggle($skKode)
	{
		$data = $this->M_master->getDataProperty('t_surat_keluar', 'skKode', $skKode, false, '');
		$data->dateConverted_tanggalSurat = date('m/d/Y', strtotime($data->skTanggalKeluar));
		echo json_encode($data);
	}

	public function suratKeluarGetDataDetilByJenis($skjID)
	{
		$response = array('status' => 'false', 'message' => 'opps something wrong..');
		if ($skjID != '') {
			$datas = $this->M_master->getDataVSuratKeluar(array('skjID' => $skjID, 'order_by' => 'skCreatedDate', 'order_by_prop' => 'desc', 'limit' => '1', 'limit_offset' => '0'));
			$response = ($datas != FALSE) ? $datas : array('status' => 'false', 'message' => 'not found');
		}
		echo json_encode($response);
	}

	public function suratKeluarGetDataAutoFormatingNomorSurat()
	{
		$response = array('status' => 'false', 'message' => 'opps something wrong');
		
		if (!$_POST) {
			echo json_encode($response);
			die;
		}

		$skNomorSuratPrev = $_POST['skNomorSuratPrev'];
		$skjID = $_POST['skjID'];
		
		if($skjID == '')
		{
			$response = array('status' => 'false', 'message' => 'Jenis Surat Keluar Harus Di isi');
			echo json_encode($response);
			die;
		}

		// get property jenis surat
		$data_surat_jenis = $this->M_master->getDataProperty('t_surat_keluar_jenis', 'skjID', $skjID, false, 'skj');

		// pecah sesuai delimiter 
		$explode_format_nomor_surat = explode($data_surat_jenis->skjFormatDelimiter, $data_surat_jenis->skjFormatNomorSurat);
		$limiter_lopping = sizeof($explode_format_nomor_surat);

		// mencari index yang akan disesuaikan dengan format penomoran surat
		for ($i=0; $i < $limiter_lopping; $i++) { 
			if ($explode_format_nomor_surat[$i] == 'noSurat') {
				$noSuratIndex = $i;
			}

			if ($explode_format_nomor_surat[$i] == 'month') {
				$monthIndex = $i;
			}

			if ($explode_format_nomor_surat[$i] == 'year') {
				$yearIndex = $i;
			}
		}

		// pecah nomor surat sebelumnya
		$explode_prev_nomor_surat = explode('/', $skNomorSuratPrev);

		if ($skNomorSuratPrev == '') {
			// sisipkan nilai selanjutnya pada nomor surat sebelumnya yang sudah dipecah
			$noSuratAppend = 0;
			$monthAppend = conDecToRoman(date('m'));
			$yearAppend = date('Y');
		}else{
			// sisipkan nilai selanjutnya pada nomor surat sebelumnya yang sudah dipecah
			if($explode_prev_nomor_surat[$yearIndex] == date('Y'))
			{
				$noSuratAppend = $explode_prev_nomor_surat[$noSuratIndex] + 1;
			}else{
				$noSuratAppend = 0;
			}
			
			$monthAppend = conDecToRoman(date('m'));
			$yearAppend = date('Y');
		}
		
		// sisipkan nilai yang baru pada format penomoran
		$explode_format_nomor_surat[$noSuratIndex] = $noSuratAppend;
		$explode_format_nomor_surat[$monthIndex] = $monthAppend;
		$explode_format_nomor_surat[$yearIndex] = $yearAppend;

		// rebuild nomor baru
		$skNomorSuratFinal = $data_surat_jenis->skjFormatPrefix.'';
		for ($a=0; $a < $limiter_lopping; $a++) { 
			$skNomorSuratFinal .= $explode_format_nomor_surat[$a];
			if ($a != ($limiter_lopping - 1)) {
				$skNomorSuratFinal .= ''.$data_surat_jenis->skjFormatDelimiter;
			}
		}

		$response = array('prev' => $skNomorSuratPrev, 'next' => $skNomorSuratFinal, 'status' => 'true', 'message' => 'oke'); 

		echo json_encode($response);
	}

	public function suratKeluarCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('skNomorSurat', 'skNomorSurat', 'required');
			$this->form_validation->set_rules('skPerihal', 'skPerihal', 'required');
			$this->form_validation->set_rules('skTanggalKeluar', 'skTanggalKeluar', 'required');
			$this->form_validation->set_rules('skTujuan', 'skTujuan', 'required');
			$this->form_validation->set_rules('instansiKode', 'instansiKode', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{
				$genSkKode = 'SK-'.date('ymd').'-'.$this->M_master->genMaster('t_surat_keluar', 'skKode', 'skCreatedDate');

				$data_row = array(
					'skKode' => $genSkKode,
					'skjID' => $_POST['skJenis'],
					'skNomorSurat' => $_POST['skNomorSurat'],
					'skPerihal' => $_POST['skPerihal'],
					'skTanggalKeluar' => date('Y-m-d', strtotime($_POST['skTanggalKeluar'])),
					'skTujuan' => $_POST['skTujuan'],
					'kodePenerima' => $_POST['instansiKode'],
					'skKeterangan' => $_POST['skKeterangan'],
					'skStatus' => 0,
					'skCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_surat_keluar', $data_row);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data surat berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function suratKeluarUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {

			$this->form_validation->set_rules('skNomorSurat', 'skNomorSurat', 'required');
			$this->form_validation->set_rules('skPerihal', 'skPerihal', 'required');
			$this->form_validation->set_rules('skTanggalKeluar', 'skTanggalKeluar', 'required');
			$this->form_validation->set_rules('skTujuan', 'skTujuan', 'required');
			$this->form_validation->set_rules('skKeterangan', 'skKeterangan', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'skNomorSurat' => $_POST['skNomorSurat'],
					'skPerihal' => $_POST['skPerihal'],
					'skTanggalKeluar' => date('Y-m-d', strtotime($_POST['skTanggalKeluar'])),
					'skTujuan' => $_POST['skTujuan'],
					'kodePenerima' => $_POST['instansiKode'],
					'skKeterangan' => $_POST['skKeterangan'],
					'skStatus' => 0,
					'skUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_surat_keluar', $data_row, 'skKode', $_POST['skKode']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data surat berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function suratKeluarDelete($skKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($skKode) {
			$data_row = array(
				'skDeletedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_surat_keluar', $data_row, 'skKode', $skKode);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data surat berhasil dihapus');
			}
		}
		echo json_encode($response);
	}

	public function disposisi()
	{
		$data['page_title'] = 'Disposisi Surat Masuk';
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'disposisi';

		$data['surats'] = $this->M_master->getDataVSuratMasuk(array('smStatusDisposisi' => '0'));
		$data['units'] = $this->M_master->getDataProperty('vunitmaster','','', true, 'unit');
		$data['kets'] = $this->M_master->getDataProperty('t_keterangan', '', '', true, 'ket');
		
		$this->load->view('modules/surat/v_disposisi', $data);
	}

	public function disposisiGetDataTabel()
	{
		$data['cntrl'] = $this;
		$data['surats'] = $this->M_master->getDataVDisposisi(array('smStatusArsip' => '0'));
		$this->load->view('modules/surat/v_disposisi_tb', $data);
	}

	public function disposisiSuratMasukGetDataTabel()
	{
		$data['cntrl'] = $this;
		$data['surats'] = $this->M_master->getDataVSuratMasuk(array('smStatus' => '1', 'smStatusArsip' => '0', 'smStatusDisposisi' => '0'));
 
		$this->load->view('modules/surat/v_surat_masuk_tb', $data);
	}

	public function disposisiGetDataSinggle($dspKode)
	{
		$data = $this->M_master->getDataProperty('t_disposisi', 'dspKode', $dspKode, false, '');
		$data->dateConverted_tanggalDsp = date('m/d/Y', strtotime($data->dspTanggalDisposisi));
		echo json_encode($data);
	}

	public function disposisiGetDataMappingUnit($dspKode)
	{
		$data = $this->M_master->getDataVDisposisiMappingUnit(array('dspKode' => $dspKode));
		echo json_encode($data);
	}

	public function disposisiGetDataKeterangan($dspKode)
	{
		$data = $this->M_master->getDataVDisposisiMappingKeterangan(array('dspKode' => $dspKode));
		return $data;
	}

	public function disposisiGetDataUnit($dspKode)
	{
		$data = $this->M_master->getDataVDisposisiMappingUnit(array('dspKode' => $dspKode));
		return $data;
	}

	public function disposisiCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		

		if ($_POST) {
			$this->form_validation->set_rules('smKode', 'smKode', 'required');
			$this->form_validation->set_rules('dspCatatan', 'dspCatatan', 'required');
			$this->form_validation->set_rules('dspTanggalDisposisi', 'dspTanggalDisposisi', 'required');
			$this->form_validation->set_rules('dspTanggalMinimalRespon', 'dspTanggalMinimalRespon', 'required');
			$this->form_validation->set_rules('keterangan[]', 'keterangan', 'required');
			$this->form_validation->set_rules('unitID[]', 'unitID', 'required');

			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{
				//print_r($_POST);

				$genDspKode = 'DSP-'.date('ymd').'-'.$this->M_master->genMaster('t_disposisi', 'dspKode', 'dspCreatedDate');

				$data_row = array(
					'dspKode' => $genDspKode,
					'smKode' => $_POST['smKode'],
					'dspCatatan' => $_POST['dspCatatan'],
					'dspTanggalDisposisi' => date('Y-m-d', strtotime($_POST['dspTanggalDisposisi'])),
					'dspTanggalMinimalRespon' => date('Y-m-d', strtotime($_POST['dspTanggalMinimalRespon'])),
					'dspStatus' => 0,
					'dspCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_disposisi', $data_row);

				if ($exce != FALSE) {

					$exce2 = $this->disposisiMappingUnit($_POST['unitID'], $genDspKode);
					$exce3 = $this->disposisiMappingKeterangan($_POST['keterangan'], $genDspKode);

					if ($exce2 != FALSE and $exce3 != FALSE) {

						$dataUpdate = array('smStatusDisposisi' => 1);
						$this->M_master->updateMaster('t_surat_masuk', $dataUpdate, 'smKode', $_POST['smKode']);

						$response = array('status' => 'true', 'message' => 'data disposisi berhasil di input');
					}else{
						$response = array('status' => 'false', 'message' => 'unit disposisi gagal di entry');
					}
				}
			}
		}
		
		echo json_encode($response);
	}

	public function disposisiMappingUnit($units = array(), $dspKode = '')
	{
		$return = false;

		if ($units) {

			$dataMapping = $this->M_master->getDataVDisposisiMappingUnit(array('dspKode' => $dspKode));

			if ($dataMapping != FALSE) {
				$this->disposisiMappingUnitDelete($dspKode);
			}

			$limiter = sizeof($units);

			for ($i=0; $i < $limiter; $i++) { 
				
				$data_row = array(
					'dspKode' => $dspKode,
					'fromUnitID' => 1,
					'unitID' => $units[$i]
				);

				$exce = $this->M_master->insertMaster('t_disposisi_unit', $data_row);

				if ($exce != FALSE) {
					$return = true;
				}
			}
		}

		return $return;
	}

	public function disposisiMappingUnitDelete($dspKode)
	{
		$return = false;

		if ($dspKode) {
			$exce = $this->M_master->deleteMaster('t_disposisi_unit', 'dspKode', $dspKode);
			if ($exce != FALSE) {
				$return = true;
			}
		}

		return $return;
	}

	public function disposisiMappingKeterangan($keterangans, $dspKode)
	{
		$return = false;

		if ($keterangans) {

			$dataMapping = $this->M_master->getDataVDisposisiMappingKeterangan(array('dspKode' => $dspKode));

			if ($dataMapping != FALSE) {
				$this->disposisiMappingKeteranganDelete($dspKode);
			}


			$limiter = sizeof($keterangans);

			for ($i=0; $i < $limiter; $i++) { 
				
				$data_row = array(
					'dspKode' => $dspKode,
					'ketID' => $keterangans[$i],
					'createdDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_disposisi_keterangan', $data_row);

				if ($exce != FALSE) {
					$return = true;
				}
			}
		}

		return $return;
	}

	public function disposisiMappingKeteranganDelete($dspKode)
	{
		$return = false;

		if ($dspKode) {
			$exce = $this->M_master->deleteMaster('t_disposisi_keterangan', 'dspKode', $dspKode);
			if ($exce != FALSE) {
				$return = true;
			}
		}

		return $return;
	}

	public function disposisiUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('dspCatatan', 'dspCatatan', 'required');
			$this->form_validation->set_rules('dspTanggalDisposisi', 'dspTanggalDisposisi', 'required');
			$this->form_validation->set_rules('dspTanggalMinimalRespon', 'dspTanggalMinimalRespon', 'required');

			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{
				
				$data_row = array(
					'smKode' => $_POST['smKode'],
					'dspCatatan' => $_POST['dspCatatan'],
					'dspTanggalDisposisi' => date('Y-m-d', strtotime($_POST['dspTanggalDisposisi'])),
					'dspTanggalMinimalRespon' => date('Y-m-d', strtotime($_POST['dspTanggalMinimalRespon'])),
					'dspStatus' => 0,
					'dspUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_disposisi', $data_row, 'dspKode', $_POST['dspKode']);

				if ($exce != FALSE) {
					$exce2 = $this->disposisiMappingUnit($_POST['unitID'], $genDspKode);

					$exce3 = $this->disposisiMappingKeterangan($_POST['keterangan'], $genDspKode);

					if ($exce2 != FALSE and $exce3 != FALSE) {

						$response = array('status' => 'true', 'message' => 'data disposisi berhasil di input');
					}else{
						$response = array('status' => 'false', 'message' => 'unit disposisi gagal di entry');
					}
				}
			}
		}
		
		echo json_encode($response);
	}

	public function disposisiDelete($dspKode, $smKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($dspKode) {
			$data_row = array(
				'dspDeletedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_disposisi', $data_row, 'dspKode', $dspKode);

			if ($exce != FALSE) {

				$dataUpdate = array('smStatusDisposisi' => 1);
						$this->M_master->updateMaster('t_surat_masuk', $dataUpdate, 'smKode', $smKode);

				$response = array('status' => 'true', 'message' => 'data disposisi berhasil dihapus');
			}
		}
		echo json_encode($response);
	}

	public function disposisiUpdateTerimaSurat($dspUnitID)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($dspUnitID) {
			$data_row = array(
				'userTerima'  =>  $this->session->userdata('username'),
				'tanggalTerima' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_disposisi_unit', $data_row, 'id', $dspUnitID);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data disposisi berhasil dikonfirmasi');
			}
		}
		echo json_encode($response);
	}

	public function disposisiPrint($dspUnitID)
	{
		$this->load->library('Pdf');
		
		$data['disposisi'] = $this->M_master->getDataVDisposisiMappingUnit(array('dspUnitID' => $dspUnitID));

		$dataKeterangan = $this->M_master->getDataVDisposisiMappingKeterangan(array('dspKode' => $data['disposisi']->dspKode));

		$keterangan = "";

		if ($dataKeterangan != false) {
			foreach ($dataKeterangan as $dk) {
				$keterangan .= '- '.$dk->ketDeskripsi.' <br>';
			}
		}

		$data['keterangan'] = $keterangan;

		$this->load->view('modules/surat/v_disposisi_pdf', $data);
	}

	public function disposisiDirektur()
	{

		$data['page_title'] = 'Disposisi';
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'direktur_disposisi';

		$this->load->view('modules/surat/v_direktur_disposisi', $data);
	}

	public function disposisiDirekturGetDataTabel()
	{
		$data['cntrl'] = $this;
		$data['surats'] = $this->M_master->getDataVSuratMasuk(array('smStatusArsip' => '0', 'orderBy_date' => 'desc'));

		$this->load->view('modules/surat/v_direktur_disposisi_tb', $data);
	}

	public function disposisiDirekturGetDataDetil($smKode)
	{
		$data = $this->M_master->getDataVSuratMasuk(array('smKode' => $smKode));
		$jml_file = $this->suratMasukGetDataCountFile($data->smKode); 
		$data->file = '<a href="#" class="btnFileView" id="'.$data->smKode.'">Uploaded File ( '.$jml_file.' )</a>';

		echo json_encode($data);
	}

	public function disposisiDirekturUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('smKode', 'smKode', 'required');
			$this->form_validation->set_rules('keterangan', 'disposisi', 'required');

			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{
				
				$data_row = array(
					'smDisposisiDirut' => $_POST['keterangan'],
					'smDisposisiDirutDate' => date('Y-m-d H:i:s'),
					'smStatus' => 1,
					'smUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_surat_masuk', $data_row, 'smKode', $_POST['smKode']);

				if ($exce != FALSE) {
					
					$response = array('status' => 'true', 'message' => 'data berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function disposisiKonfirmasi()
	{
		$data['page_title'] = 'Konfirmasi Disposisi';
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'konfirmasi_disposisi';

		$data['units'] = $this->M_master->getDataProperty('vunitmaster','','', true, 'unit');
		$this->load->view('modules/surat/v_konfirmasi_disposisi', $data);
	}

	public function disposisiKonfirmasiGetData($unitID)
	{
		$data['cntrl'] = $this;
		$data['disps'] = $this->M_master->getDataVDisposisiMappingUnit(array('unitID' => $unitID, 'order_by' => 'smTanggalSurat', 'order_type' => 'DESC'));
		$this->load->view('modules/surat/v_konfirmasi_disposisi_tb', $data);
	}

	public function disposisiKonfirmasiGetDataDetil($id)
	{
		$data = $this->M_master->getDataVDisposisiMappingUnit(array('dspUnitID' => $id));

		$dataKeterangan = $this->M_master->getDataVDisposisiMappingKeterangan(array('dspKode' => $data->dspKode));

		$keterangan = "";

		if ($dataKeterangan != false) {
			foreach ($dataKeterangan as $dk) {
				$keterangan .= '- '.$dk->ketDeskripsi.' <br>';
			}
		}

		$data->keteranganDisposisi = $keterangan;

		echo json_encode($data);
	}

	public function disposisiKonfirmasiCreate()
	{
		//print_r($_POST);
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('keteranganKonfirmasi', 'keteranganKonfirmasi', 'required');

			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{
				
				$data_row = array(
					'catatanKonfirmasi' => $_POST['keteranganKonfirmasi'],
					'tanggalKonfirmasi' => date('Y-m-d H:i:s'),
					'userResponder' => $this->session->userdata('username')
				);

				$exce = $this->M_master->updateMaster('t_disposisi_unit', $data_row, 'id', $_POST['id']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data disposisi berhasil di update');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function disposisiTransfer()
	{
		//print_r($_POST);
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('id', 'id', 'required');
			$this->form_validation->set_rules('fromUnitID', 'fromUnitID', 'required');
			$this->form_validation->set_rules('toUnitID', 'toUnitID', 'required');
			$this->form_validation->set_rules('keteranganKonfirmasi', 'keteranganKonfirmasi', 'required');

			if ($this->form_validation->run() == FALSE) {
				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');
			}else{

				$validasi_disposisi = $this->disposisiTransferValidasi($_POST['id'], $_POST['fromUnitID'], $_POST['toUnitID']);

				if ($validasi_disposisi == FALSE) {
					
					$data_row = array(
						'catatanKonfirmasi' => $_POST['keteranganKonfirmasi'],
						'tanggalKonfirmasi' => date('Y-m-d H:i:s'),
						'userResponder' => $this->session->userdata('username')
					);

					$exce = $this->M_master->updateMaster('t_disposisi_unit', $data_row, 'id', $_POST['id']);
					
					if ($exce != FALSE) {
						$create_transfer = $this->disposisiTransferCreate($_POST['id'], $_POST['fromUnitID'], $_POST['toUnitID']);
						if ($create_transfer != FALSE) {
							$response = array('status' => 'true', 'message' => 'data disposisi berhasil di update');
						}
					}
				}else{
					$response = array('status' => 'false', 'message' => 'tidak dapat disposisi ke unit yang sudah mendapat disposisi surat yang sama');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function disposisiTransferValidasi($id, $fromUnitID, $toUnitID)
	{
		$return = FALSE;
		
		$disposisi_unit = $this->M_master->getDataVDisposisiMappingUnit(array('dspUnitID' => $id));

		if ($disposisi_unit != FALSE) {
			

			$cek_unit = $this->M_master->getDataVDisposisiMappingUnit(array('dspKode' => $disposisi_unit->dspKode, 'unitID' => $toUnitID));

			if ($cek_unit != FALSE) {
				$return = TRUE;
			}
		}

		return $return;
	}

	public function disposisiTransferCreate($id, $fromUnitID, $toUnitID)
	{
		$return = FALSE;

		$disposisi_unit = $this->M_master->getDataVDisposisiMappingUnit(array('dspUnitID' => $id));

		if ($disposisi_unit != FALSE) {
			
			$data_row = array(
				'dspKode' => $disposisi_unit->dspKode,
				'fromUnitID' => $fromUnitID,
				'unitID' => $toUnitID
			);

			$exce = $this->M_master->insertMaster('t_disposisi_unit', $data_row);

			if ($exce != FALSE) {
				$return = TRUE;
			}
		}

		return $return;
	}

	public function fileUploader($reffKode)
	{
		$data['page_title'] = 'Upload File '.$reffKode;
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'surat_masuk';

		$explode_kode = explode('-', $reffKode);
		$preffix = $explode_kode[0];

		if ($preffix == 'SM') {
			$data['surat'] = $this->M_master->getDataVSuratMasuk(array('smKode' => $reffKode));
		}

		if ($preffix == 'SK') {
			$data['surat'] = $this->M_master->getDataVSuratKeluar(array('skKode' => $reffKode));
		}

		$data['reffKode'] = $reffKode;
		$data['jenis_surat'] = $preffix;
		$this->load->view('modules/surat/v_file_uploader', $data);
	}

	public function fileUploaderGetDataTb($reffKode)
	{
		$data['files'] = $this->M_master->getDataFile(array('refKode' => $reffKode));
		$this->load->view('modules/surat/v_file_uploader_tb', $data);
	}

	public function fileUploaderSave()
	{
		$response = array('status' => 'false', 'message' => 'opps.. something wrong');

		// cek apakah variabel POST terisi atau tidak
		if ($_POST) {

			//$response = $_FILES;
			// cek apakah file empty atau tidak
			if (!empty($_FILES)) {

				//$response = $_FILES;
				// jalankan fungsi dekripsi untuk mendapatkan nilai asli dari reg_kode
				$reffKode = $_POST['reffKode'];
				$files = $_FILES['files'];

				// triger fungsi file uploader dengan mengiri properti berupa path upload, reg kode, dan file yang dimaksud.
				$upload = $this->file_uploader($reffKode, $files);

				// jika upload berhasil maka return status true
				if ($upload != false) {
					$response = array('status' => 'true', 'message' => 'baik');
				}else{
					$response = array('message' => 'gagal upload');
				}

			}else{
				$response = array('message' => 'tidak ada file');
			}
		}

		// tampilkan nilai return berupa json ke client
		echo json_encode($response);
	}

	/* -- fungsi uploader file -- */
	public function file_uploader($reffernce_kode, $files)
	{
		$return = false;
		// inisiasi configurasi untuk file upload
		$config = array(
            'upload_path'   => 'assets/uploads/surat/', // path folder upload
            'allowed_types' => 'doc|docx|pdf|jpg|jpeg|png', // file yang di izinkan
            'overwrite'     => 1,                       
        );

        $this->load->library('upload', $config);

        $images = array();

		// perulangan pengecekan setiap inputan karena sistem menangkap multi upload
        foreach ($files['name'] as $key => $image) {

			// generate kode file
			$file_kode = 'FILE'.date('ymd').''.$this->M_master->genMaster('t_file', 'fileKode', 'fileCreatedDate');

            $_FILES['files[]']['name']= $files['name'][$key];
            $_FILES['files[]']['type']= $files['type'][$key];
            $_FILES['files[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['files[]']['error']= $files['error'][$key];
            $_FILES['files[]']['size']= $files['size'][$key];

            $fileName = $file_kode .'-'. $image;
            $fileName = str_replace(' ', '-', $fileName);
			$fileName = preg_replace('/[^A-Za-z0-9\-]/', '', $fileName);
			$fileName = preg_replace('/-+/', '-', $fileName);
			$fileName = $fileName.'.'.pathinfo($image, PATHINFO_EXTENSION);

            $images[] = $fileName;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);
            // lakukan upload data file
            if ($this->upload->do_upload('files[]')) {
                $this->upload->data();
                // simpan property file di database.

                $data_file = array(
                	'fileKode' => $file_kode,
                	'refKode' => $reffernce_kode,
                	'fileName' => $fileName,
                	'fileType' => $_FILES['files[]']['type'],
                	'fileSize' => $_FILES['files[]']['size'],
                	'fileExt' => $_FILES['files[]']['type'],
                	'fileDirectory' => 'assets/uploads/surat/'.$fileName,
                	'fileCreatedDate' => date('Y-m-d H:i:s'),
                	'fileUploadedBy' => $this->session->userdata('username'),		                
                );

                $query = $this->M_master->insertMaster('t_file', $data_file);
            	if ($query != false) {
            		$return = true;
            	}
            } 
        }

        return $return;
	}

	public function fileRemove($fileKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($fileKode) {

			$file = $this->M_master->getDataFile(array('fileKode' => $fileKode));

			$fileName = $file->fileName;
			$fileDirectory = $_SERVER['DOCUMENT_ROOT'].'/surat_bros/'.$file->fileDirectory;

			// remove file in directory
			if (unlink($fileDirectory)) {
				$exce = $this->M_master->deleteMaster('t_file', 'fileKode', $fileKode);

				$response = array('status' => 'true', 'message' => 'file has been deleted');
			}
		}

		echo json_encode($response);
	}

	public function fileDownloader($filename)
	{
		$file_name_new = str_replace('%20', '_', $filename);
		//echo $file_name_new;
		if(force_download('./assets/uploads/surat/'.$file_name_new, NULL))
		{
			echo "sukses";
		}else{
			echo "failed";

		}
	}
	
	public function filePdfViewer($fileKode)
	{
		$data['file'] = $this->M_master->getDataFile(array('fileKode' => $fileKode));
		$this->load->view('modules/surat/v_pdf_viewer', $data);
	}

	public function arsipSuratMasuk()
	{
		$data['page_title'] = 'Arsip Surat Masuk';
		$data['page_modul'] = 'surat';
		$data['page_tree'] = 'surat';
		$data['page_val'] = 'arsip_surat_masuk';

		$this->load->view('modules/surat/v_arsip_surat', $data);
	}

	public function arsipSuratMasukGetDataTb()
	{
		$data['cntrl'] = $this;
		$data['surats'] = $this->M_master->getDataVSuratMasuk(array('smStatusArsip' => 1));
		$this->load->view('modules/surat/v_arsip_surat_tb', $data);
	}

	public function arsipSuratMasukValidasiDisposisi($smKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($smKode != '') {

			$data_dsp_unit = $this->M_master->getDataVDisposisiMappingUnit(array('smKode' => $smKode));

			if ($data_dsp_unit != FALSE) {

				foreach ($data_dsp_unit as $dsp) {

					if ($dsp->tanggalKonfirmasi == null) {
						
						$response = array('status' => 'false', 'message' => 'terdapat unit yang masih belum merespon disposisi, surat tidak dapat di arsipkan');

						break;
					}


					$response = array('status' => 'true', 'message' => 'oke');
				}

			}else{
				$response = array('status' => 'true', 'message' => 'tidak ada unit yang disposisi, apakah anda yakin untuk mengarsipkan surat ini? ');
			}
		}
		
		echo json_encode($response);
	}

	public function arsipSuratMasukSave()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {

			$this->form_validation->set_rules('smKode', 'smKode', 'required');
			$this->form_validation->set_rules('smNomorSurat', 'smNomorSurat', 'required');
			$this->form_validation->set_rules('dspCatatan', 'dspCatatan', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'smStatusArsip' => '1',
					'smTanggalArsip' => date('Y-m-d H:i:s'),
					'smKeteranganArsip' => $_POST['dspCatatan'],
					'smUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_surat_masuk', $data_row, 'smKode', $_POST['smKode']);
				
				$disposisi = ['dspStatus' => 1, 'dspUpdatedDate' => date('Y-m-d h:i:s')];
				$exce = $this->M_master->updateMaster('t_disposisi', $disposisi, 'smKode', $_POST['smKode']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data surat berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function arsipSuratMasukReOpen($smKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($smKode != '') {

			$data_row = array(
				'smStatusArsip' => '0',
				'smTanggalArsip' => null,
				'smKeteranganArsip' => null,
				'smUpdatedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_surat_masuk', $data_row, 'smKode', $smKode);

			$disposisi = ['dspStatus' => 0, 'dspUpdatedDate' => date('Y-m-d h:i:s')];
			$exce = $this->M_master->updateMaster('t_disposisi', $disposisi, 'smKode', $smKode);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data arsip berhasil di re open');
			}
		}
		
		echo json_encode($response);
	}
}
