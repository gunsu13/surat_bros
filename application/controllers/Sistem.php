<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistem extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->model('M_master');	
	}

	public function setting()
	{
		$data['page_title'] = 'Dashboard';
		$data['page_modul'] = 'dashboard';
		$data['page_tree'] = 'dashboard';
		$data['page_val'] = 'dashboard';

		$this->load->view('modules/sistem/v_setting', $data);
	}

	public function settingGetDataDetil($company_kode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($company_kode != '') {
			$data_setting = $this->M_master->getDataSetting(array('company_kode' => $company_kode));
			$response = $data_setting;
		}
		
		echo json_encode($response);
	}

	public function settingUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('company_name', 'company_name', 'required');
			$this->form_validation->set_rules('company_address', 'company_address', 'required');
			$this->form_validation->set_rules('company_telp', 'company_telp', 'required');
			$this->form_validation->set_rules('comapny_email', 'comapny_email', 'required');
			$this->form_validation->set_rules('company_web', 'company_web', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');
			}else{
				$data_row = array(
					'company_name' => $_POST['company_name'],
					'company_address' => $_POST['company_address'],
					'company_telp' => $_POST['company_telp'],
					'comapny_email' => $_POST['comapny_email'],
					'company_web' => $_POST['company_web'],
					'company_logo' => $_POST['company_logo'],
					'company_logo_dir' => $_POST['company_logo_dir']
				);

				$exce = $this->M_master->updateMaster('t_settings', $data_row, 'company_kode', $_POST['company_kode']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'setting has been saved');
				}
			}
		}

		echo json_encode($response);
	}
}