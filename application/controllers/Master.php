<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->model('M_master');	
	}

	public function divisi()
	{
		$data['page_title'] = 'Master Data Divisi';
		$data['page_modul'] = 'master';
		$data['page_tree'] = 'master';
		$data['page_val'] = 'divisi';

		$this->load->view('modules/master/v_divisi', $data);
	}

	public function divisiGetDataTabel()
	{
		$data['divisis'] = $this->M_master->getDataProperty('t_divisi','','', true, 'divisi');
		//print_r($data['divisis']);
		$this->load->view('modules/master/v_divisi_tb', $data);
	}
	
	public function divisiGetDataSinggle($divisiID)
	{
		$data = $this->M_master->getDataProperty('t_divisi', 'divisiID', $divisiID, false, '');
		echo json_encode($data);
	}

	public function divisiCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('divisiNama', 'Nama Divisi', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');
			}else{
				$data_row = array(
					'divisiNama' => $_POST['divisiNama'],
					'divisiKeterangan' => $_POST['divisiKeterangan'],
					'divisiKepala' => $_POST['divisiKepala'],
					'divisiCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_divisi', $data_row);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data divisi berhasil di input');
				}
			}
		}
		echo json_encode($response);
	}

	public function divisiUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('divisiNama', 'Nama Divisi', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');
			}else{
				$data_row = array(
					'divisiNama' => $_POST['divisiNama'],
					'divisiKeterangan' => $_POST['divisiKeterangan'],
					'divisiKepala' => $_POST['divisiKepala'],
					'divisiUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_divisi', $data_row, 'divisiID', $_POST['divisiID']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data divisi berhasil di ubah');
				}
			}
		}
		echo json_encode($response);

	}

	public function divisiDelete($divisiID)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($divisiID != '') {
			
			$data_row = array(
				'divisiDeletedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_divisi', $data_row, 'divisiID', $divisiID);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data divisi berhasil di ubah');
			}
		}
		echo json_encode($response);
		
	}

	public function unit()
	{
		$data['page_title'] = 'Master Data Unit';
		$data['page_modul'] = 'master';
		$data['page_tree'] = 'master';
		$data['page_val'] = 'unit';

		$data['divisis'] = $this->M_master->getDataProperty('t_divisi','','', true, 'divisi');
		
		$this->load->view('modules/master/v_unit', $data);
	}
		
	public function unitGetDataTabel()
	{
		$data['units'] = $this->M_master->getDataProperty('vunitmaster','','', true, 'unit');
		$this->load->view('modules/master/v_unit_tb', $data);
	}	

	public function unitGetDataSinggle($unitID)
	{
		$data = $this->M_master->getDataProperty('t_unit', 'unitID', $unitID, false, '');
		echo json_encode($data);
	}

	public function unitCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('divisiID', 'Divisi', 'required');
			$this->form_validation->set_rules('unitNama', 'unitNama', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'divisiID' => $_POST['divisiID'],
					'unitNama' => $_POST['unitNama'],
					'unitKeterangan' => $_POST['unitKeterangan'],
					'unitKepala' => $_POST['unitKepala'],
					'unitCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_unit', $data_row);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data unit berhasil di input');
				}
			}
		}

		echo json_encode($response);
	}

	public function unitUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {

			$this->form_validation->set_rules('divisiID', 'Divisi', 'required');
			$this->form_validation->set_rules('unitNama', 'unitNama', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'divisiID' => $_POST['divisiID'],
					'unitNama' => $_POST['unitNama'],
					'unitKeterangan' => $_POST['unitKeterangan'],
					'unitKepala' => $_POST['unitKepala'],
					'unitUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_unit', $data_row, 'unitID', $_POST['unitID']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data unit berhasil di update');
				}
			}
		}

		echo json_encode($response);
	}

	public function unitDelete($unitID)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($unitID != '') {
			
			$data_row = array(
				'unitDeletedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_unit', $data_row, 'unitID', $unitID);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data divisi berhasil di ubah');
			}
		}

		echo json_encode($response);
	}

	public function instansi()
	{
		$data['page_title'] = 'Master Data Instansi';
		$data['page_modul'] = 'master';
		$data['page_tree'] = 'master';
		$data['page_val'] = 'instansi';

		$this->load->view('modules/master/v_instansi', $data);
	}

	public function instansiGetDataTabel()
	{
		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');
		$this->load->view('modules/master/v_instansi_tb', $data);
	}

	public function instansiGetDataSinggle($instansiKode)
	{
		$data = $this->M_master->getDataProperty('t_instansi', 'instansiKode', $instansiKode, false, '');
		echo json_encode($data);
	}

	public function instansiCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('instansiKode', 'Instansi Kode', 'required|is_unique[t_instansi.instansiKode]',
			array(
					'required'      => '%s harus di isi.',
					'is_unique'     => '%s ini sudah tersedia.'
			));
			
			$this->form_validation->set_rules('instansiNama', 'instansiNama', 'required');
			$this->form_validation->set_rules('instansiAlamat', 'instansiAlamat', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'instansiKode' => $_POST['instansiKode'],
					'instansiNama' => $_POST['instansiNama'],
					'instansiAlamat' => $_POST['instansiAlamat'],
					'instansiNomorTlp' => $_POST['instansiNomorTlp'],
					'instansiEmail' => $_POST['instansiEmail'],
					'instansiCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_instansi', $data_row);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data instansi berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function instansiUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('instansiKode', 'Instansi Kode', 'required');
			$this->form_validation->set_rules('instansiNama', 'instansiNama', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'instansiNama' => $_POST['instansiNama'],'instansiAlamat' => $_POST['instansiAlamat'],
					'instansiNomorTlp' => $_POST['instansiNomorTlp'],
					'instansiEmail' => $_POST['instansiEmail'],
					'instansiUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_instansi', $data_row, 'instansiKode', $_POST['instansiKode']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data instansi berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function instansiDelete($instansiKode)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($instansiKode != '') {
			
			$data_row = array(
				'instansiDeletedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_instansi', $data_row, 'instansiKode', $instansiKode);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data instansi berhasil di ubah');
			}
		}

		echo json_encode($response);
	}

	public function keteranganDisposisi()
	{
		$data['page_title'] = 'Master Data Keterangan Disposisi';
		$data['page_modul'] = 'master';
		$data['page_tree'] = 'master';
		$data['page_val'] = 'keterangan';

		$this->load->view('modules/master/v_keterangan', $data);
	}

	public function keteranganDisposisiGetDataTabel()
	{
		$data['kets'] = $this->M_master->getDataProperty('t_keterangan', '', '', true, 'ket');
		$this->load->view('modules/master/v_keterangan_tb', $data);
	}

	public function keteranganDisposisiGetDataSinggle($ketID)
	{
		$data = $this->M_master->getDataProperty('t_keterangan', 'ketID', $ketID, false, 'ket');
		echo json_encode($data);
	}

	public function keteranganDisposisiCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('ketDeskripsi', 'ketDeskripsi', 'required');
			$this->form_validation->set_rules('ketDeskripsiDetil', 'ketDeskripsiDetil', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'ketDeskripsi' => $_POST['ketDeskripsi'],
					'ketDeskripsiDetil' => $_POST['ketDeskripsiDetil'],
					'ketCreatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->insertMaster('t_keterangan', $data_row);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data keterangan disposisi berhasil di input');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function keteranganDisposisiUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {
			$this->form_validation->set_rules('ketDeskripsi', 'ketDeskripsi', 'required');
			$this->form_validation->set_rules('ketDeskripsiDetil', 'ketDeskripsiDetil', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$data_row = array(
					'ketDeskripsi' => $_POST['ketDeskripsi'],
					'ketDeskripsiDetil' => $_POST['ketDeskripsiDetil'],
					'ketUpdatedDate' => date('Y-m-d H:i:s')
				);

				$exce = $this->M_master->updateMaster('t_keterangan', $data_row, 'ketID', $_POST['ketID']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data keterangan disposisi berhasil di ubah');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function keteranganDisposisiDelete($ketID)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($ketID != '') {
			
			$data_row = array(
				'ketDeletedDate' => date('Y-m-d H:i:s')
			);

			$exce = $this->M_master->updateMaster('t_keterangan', $data_row, 'ketID', $ketID);


			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data keterangan disposisi berhasil di hapus');
			}
		}

		echo json_encode($response);
	}

	public function user()
	{
		$data['page_title'] = 'Manajemen User';
		$data['page_modul'] = 'master';
		$data['page_tree'] = 'master';
		$data['page_val'] = 'user';

		$data['units'] = $this->M_master->getDataProperty('t_unit', '', '', true, 'unit');

		$this->load->view('modules/master/v_user', $data);
	}

	public function userGetData()
	{
		$data['users'] = $this->M_master->getDataProperty('vusermaster', '','', true, '');
		$this->load->view('modules/master/v_user_tb', $data);
	}

	public function userGetDataDetil($username)
	{ 
		$data_user = $this->M_master->getDataProperty('vusermaster', 'username', $username, false, '');
		echo json_encode($data_user);
	}

	public function userCreate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {

			$this->form_validation->set_rules('username', 'username', 'required|is_unique[t_user.username]');
			$this->form_validation->set_rules('display_name', 'display_name', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');
			$this->form_validation->set_rules('repassword', 'repassword', 'required');
			$this->form_validation->set_rules('unitID', 'unitID', 'required');
			$this->form_validation->set_rules('level', 'level', 'required');
			$this->form_validation->set_rules('status', 'status', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				$password = $_POST['password'];
				$repassword = $_POST['repassword'];

				$hash = $this->userHasPassword($password);

				if (password_verify($repassword, $hash)) {

					$data_row = array(
						'username' => $_POST['username'],
						'display_name' => $_POST['display_name'],
						'password' => $hash,
						'unitID' => $_POST['unitID'],
						'level' => $_POST['level'],
						'status' => $_POST['status'],
						'createdDate' => date('Y-m-d H:i:s')
					);

					$exce = $this->M_master->insertMaster('t_user', $data_row);

					if ($exce != FALSE) {
						$response = array('status' => 'true', 'message' => 'data user berhasil di input');
					}
				}
			}
		}
		
		echo json_encode($response);
	}

	public function userUpdate()
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($_POST) {

			$this->form_validation->set_rules('display_name', 'display_name', 'required');
			$this->form_validation->set_rules('username', 'username', 'required');
			$this->form_validation->set_rules('unitID', 'unitID', 'required');
			$this->form_validation->set_rules('level', 'level', 'required');
			$this->form_validation->set_rules('status', 'status', 'required');
			
			if ($this->form_validation->run() == FALSE) {

				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');

			}else{

				if (isset($_POST['password'])) {

					$password = $_POST['password'];
					$repassword = $_POST['repassword'];

					$hash = $this->userHasPassword($password);

					if (password_verify($repassword, $hash)) {

						$data_row = array(
							'display_name' => $_POST['display_name'],
							'password' => $hash,
							'unitID' => $_POST['unitID'],
							'level' => $_POST['level'],
							'status' => $_POST['status'],
							'updatedDate' => date('Y-m-d H:i:s')
						);
					}
				}else{
				    $data_row = array(
						'display_name' => $_POST['display_name'],
						'unitID' => $_POST['unitID'],
						'level' => $_POST['level'],
						'status' => $_POST['status'],
						'updatedDate' => date('Y-m-d H:i:s')
					);
				}

				$exce = $this->M_master->updateMaster('t_user', $data_row, 'username', $_POST['username']);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'data user berhasil di update');
				}
			}
		}
		
		echo json_encode($response);
	}

	public function userDeleted($username)
	{
		$response = array('status' => 'false', 'message' => 'something wrong');
		
		if ($username != '') {

		    $data_row = array(
				'status' => 0,
				'deletedDate' => date('Y-m-d H:i:s')
			);
				
			$exce = $this->M_master->updateMaster('t_user', $data_row, 'username', $username);

			if ($exce != FALSE) {
				$response = array('status' => 'true', 'message' => 'data user berhasil di hapus');
			}		
		}
		
		echo json_encode($response);
	}

	private function userHasPassword($password)
	{
	   return password_hash($password, PASSWORD_BCRYPT);
	}

}
