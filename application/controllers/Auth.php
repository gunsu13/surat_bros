<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->model('M_master');	
	}

	public function index()
	{
		$this->load->view('modules/sistem/v_login');
	}

	public function auth_do()
	{
		$response = array();

		if ($_POST) {
			$this->form_validation->set_rules('username', 'username', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');
			
			if ($this->form_validation->run() == FALSE) {
				$this->form_validation->set_error_delimiters('','');
				$response = array('status' => 'false', 'message' => ''.validation_errors().'');
			}else{
				$data_setting = $this->M_master->getDataSetting(array('company_kode' => 'BROS'));
				$data_user = $this->M_master->getDataProperty('vusermaster', 'username', $_POST['username'], false, '');

				if ($data_user == FALSE) {
					$response = array('status' => 'false', 'message' => 'User tidak terdaftar. silahkan kontak sistem administrator');
				}else{
					if ($data_user->status == 0) {
						$response = array('status' => 'false', 'message' => 'User tidak aktif. silahkan kontak sistem administrator');
					}else{
						$password 	= $_POST['password'];
						$hash 		= $data_user->password;

						if (password_verify($password, $hash)) {
							// store session
							$this->session->set_userdata('username', $data_user->username);
							$this->session->set_userdata('display_name', $data_user->display_name);
							$this->session->set_userdata('level', $data_user->level);
							$this->session->set_userdata('unitID', $data_user->unitID);


							$this->session->set_userdata('company_kode', $data_setting->company_kode);
							$response = array('status' => 'true', 'message' => 'oke');
						}else{
							$response = array('status' => 'false', 'message' => 'Password yang dimasukan salah');
						}
					}
				}
			}
		}else{
			$response = array('status' => 'false', 'message' => 'nothing to do here..');
		}

		echo json_encode($response);
	}

	public function auth_destroy()
	{
		$this->session->unset_userdata(array(
			'username',
			'display_name',
			'level',
			'unitID'
		));

		$this->session->sess_destroy();

		redirect('Auth');
	}

	public function reset_password($username)
	{
		$response = array();

		if (isset($username)) {

			$data_user = $this->M_master->getDataProperty('vusermaster', 'username', $username, false, '');

			if ($data_user == FALSE) {
				$response = array('status' => 'false', 'message' => 'user tidak terdaftar');	
			}else{

				$password_restart = '12345';

				$hash = $this->userHasPassword($password_restart);

				$data_update = array(
					'password' => $hash
				);

				$exce = $this->M_master->updateMaster('t_user', $data_update, 'username', $username);

				if ($exce != FALSE) {
					$response = array('status' => 'true', 'message' => 'user password berhasil diubah');	
				}else{
					$response = array('status' => 'false', 'message' => 'error update');	
				}
			}
		}else{
			$response = array('status' => 'false', 'message' => 'nothing to do here');	
		}

		echo json_encode($response);
	}

	private function userHasPassword($password)
	{
	   return password_hash($password, PASSWORD_BCRYPT);
	}
}