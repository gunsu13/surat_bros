<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->model('M_master');	
	}

	public function index()
	{

		$data['page_title'] = 'Dashboard';
		$data['page_modul'] = 'dashboard';
		$data['page_tree'] = 'dashboard';
		$data['page_val'] = 'dashboard';

		$this->load->view('modules/dashboard/v_dashboard', $data);
	}
}