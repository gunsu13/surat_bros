<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct() {
	    parent::__construct();
	    $this->load->model('M_master');	
	}

	public function surat_masuk()
	{
		$data['page_title'] = 'Informasi Surat Masuk';
		$data['page_modul'] = 'laporan';
		$data['page_tree'] = 'laporan';
		$data['page_val'] = 'lap_surat_masuk';

		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');
		$this->load->view('modules/report/v_laporan_surat_masuk', $data);
	}

	public function surat_masuk_getDataTb()
	{
		if ($_POST) {

			$pecah = explode('-', $_POST['dateFiltered']);
			$date_start = date('Y-m-d', strtotime($pecah[0]));
			$date_end = date('Y-m-d', strtotime($pecah[1]));

			$data['surats'] = $this->M_master->getDataVSuratMasuk(array('smStatusDisposisi' => $_POST['smStatusDisposisi'], 'instansiKode' => $_POST['instansiKode'], 'date_filter' => 'true', 'date_start' => $date_start, 'date_end' => $date_end));

			$this->load->view('modules/report/v_laporan_surat_masuk_tb', $data);
		}
	}

	public function surat_keluar()
	{
		$data['page_title'] = 'Laporan Surat Keluar';
		$data['page_modul'] = 'laporan';
		$data['page_tree'] = 'laporan';
		$data['page_val'] = 'lap_surat_keluar';

		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');
		$this->load->view('modules/report/v_laporan_surat_keluar', $data);
	}

	public function surat_keluar_getDataTb()
	{
		if ($_POST) {

			$pecah = explode('-', $_POST['dateFiltered']);
			$date_start = date('Y-m-d', strtotime($pecah[0]));
			$date_end = date('Y-m-d', strtotime($pecah[1]));

			$data['surats'] = $this->M_master->getDataVSuratKeluar(array('skStatus' => $_POST['skStatus'], 'instansiKode' => $_POST['instansiKode'], 'date_filter' => 'true', 'date_start' => $date_start, 'date_end' => $date_end));

			$this->load->view('modules/report/v_laporan_surat_keluar_tb', $data);
		}
	}

	public function disposisi()
	{
		$data['page_title'] = 'Laporan Disposisi';
		$data['page_modul'] = 'laporan';
		$data['page_tree'] = 'laporan';
		$data['page_val'] = 'lap_disposisi';

		$this->load->view('modules/report/v_laporan_disposisi', $data);
	}

	public function disposisi_getDataTb()
	{
		if ($_POST) {
			$data['cntrl'] = $this;
			$pecah = explode('-', $_POST['dateFiltered']);
			$date_start = date('Y-m-d', strtotime($pecah[0]));
			$date_end = date('Y-m-d', strtotime($pecah[1]));

			$data['surats'] = $this->M_master->getDataVDisposisi(array('dspStatus' => $_POST['dspStatus'], 'date_filter' => 'true', 'date_start' => $date_start, 'date_end' => $date_end));

			$this->load->view('modules/report/v_laporan_disposisi_tb', $data);
		}
	}

	public function disposisiGetDataKeterangan($dspKode)
	{
		$data = $this->M_master->getDataVDisposisiMappingKeterangan(array('dspKode' => $dspKode));
		return $data;
	}

	public function disposisiGetDataUnit($dspKode)
	{
		$data = $this->M_master->getDataVDisposisiMappingUnit(array('dspKode' => $dspKode));
		return $data;
	}

	public function disposisi_konfirmasi()
	{
		$data['page_title'] = 'Laporan Konfirmasi Disposisi';
		$data['page_modul'] = 'laporan';
		$data['page_tree'] = 'laporan';
		$data['page_val'] = 'lap_disposisi_konfirmasi';

		$data['divisis'] = $this->M_master->getDataProperty('t_divisi','','', true, 'divisi');
		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');
		$data['units'] = $this->M_master->getDataProperty('vunitmaster','','', true, 'unit');

		$this->load->view('modules/report/v_laporan_disposisi_konfirmasi', $data);
	}

	public function disposisi_konfirmasiGetDataTb()
	{
		if ($_POST) {
			$data['cntrl'] = $this;
			$pecah = explode('-', $_POST['dateFiltered']);
			$date_start = date('Y-m-d', strtotime($pecah[0]));
			$date_end = date('Y-m-d', strtotime($pecah[1]));

			$isNotNullFiltered = 'tanggalKonfirmasi';

			if (isset($_POST['showNotConfirmed'])) {
				$isNotNullFiltered = '';
			}

			$data['surats'] = $this->M_master->getDataVDisposisiMappingUnit(array('instansiKode' => $_POST['instansiKode'], 'unitID' => $_POST['unitID'],'divisiID' => $_POST['divisiID'], 'date_filter' => 'true', 'date_start' => $date_start, 'date_end' => $date_end, 'isNotNull' => $isNotNullFiltered));

			$this->load->view('modules/report/v_laporan_disposisi_konfirmasi_tb', $data);
		}
	}

	public function arsip_surat_masuk()
	{
		$data['page_title'] = 'Laporan Konfirmasi Disposisi';
		$data['page_modul'] = 'laporan';
		$data['page_tree'] = 'laporan';
		$data['page_val'] = 'lap_disposisi_konfirmasi';

		$data['instansis'] = $this->M_master->getDataProperty('t_instansi','','', true, 'instansi');

		$this->load->view('modules/report/v_laporan_arsip', $data);
	}

	public function arsip_surat_masuk_tb()
	{
		if ($_POST) {
			$data['cntrl'] = $this;

			$pecah = explode('-', $_POST['dateFiltered']);
			$date_start = date('Y-m-d', strtotime($pecah[0]));
			$date_end = date('Y-m-d', strtotime($pecah[1]));

			$data['arsips'] = $this->M_master->getDataVSuratMasuk(array('instansiKode' => $_POST['instansiKode'], 'smStatusArsip' => 1, 'date_filter' => 'true', 'date_start' => $date_start, 'date_end' => $date_end));

			$this->load->view('modules/report/v_laporan_arsip_tb', $data);
		}
	}
}